import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  userDetails:any;
  total_order_amnt:any;
  current_month_order_amnt:any;
  total_order_count:any;
  total_review_count:any;
  activeMenu:any;
  
  constructor(private commonservice:CommonService,private route: Router,private http: HttpClient) { }
  
  ngOnInit(): void {
  this.commonservice.loggeduser.subscribe(user => {
   this.userDetails = user; 
   this.total_order_count=this.userDetails.total_pending_order;
  });  
  let  body={id:this.userDetails.id};
   this.commonservice.postData(body,"seller/orderFigure").subscribe(data => {
    if(data.status)
    {
      this.total_order_amnt=data.total_order_amnt;
      this.current_month_order_amnt=data.current_month_order_amnt;
     
      this.total_review_count=data.total_review_count;
    }

    },
    err => {
      console.log("Error occured.",err)
    });     
      
  }

}
