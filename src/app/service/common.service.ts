import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject,BehaviorSubject  } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  upload_type:any;
  token:any;
  constructor(private http: HttpClient) { 
 
  }
  public loggeduser = new BehaviorSubject <any>(JSON.parse(window.localStorage.getItem('userDetails')));
  public uploadPath = new BehaviorSubject <any>('');
  public uploadSetting = new BehaviorSubject <any>('');
  public totalcartItem = new BehaviorSubject <any>(0);
  
  setfilePath(rbody: any)
  {
      this.uploadPath.next(rbody);  
  }
   SetUser() {

      let currentuser = JSON.parse(window.localStorage.getItem('userDetails')); 
      this.loggeduser.next(currentuser);  

    }
  setuploadType(upload_type)
  {
     this.upload_type=upload_type;
  }
  returnuploadType()
  {
     return this.upload_type;
  }
  
  postData(body: any,url: any) : Observable<any>{
   this.token=window.localStorage.getItem('authtoken');    
  if(this.token==null)
  {
      this.token="";
  }  
  const webservice_path = 'https://dev1.ivantechnology.in/cliqpack/api/v1/'; 
  let form_data = new FormData();

for ( var key in body ) {
    form_data.append(key, body[key]);
}
    let httpOptions =  {
        headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization':this.token
      })
    }
    return this.http.post(webservice_path+url,form_data,httpOptions)
   
  }
  
  postDataRaw(body: any,url: any) : Observable<any>{
  const webservice_path = 'https://dev1.ivantechnology.in/cliqpack/api/v1/'; 
  
    let httpOptions =  {
        headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      })
    }
    return this.http.post(webservice_path+url,body,httpOptions)
   
  }
  
  
  
}
