import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-customer-sidebar',
  templateUrl: './customer-sidebar.component.html',
  styleUrls: ['./customer-sidebar.component.css']
})
export class CustomerSidebarComponent implements OnInit {
  userDetails:any;
 
  activeMenu:any;
  constructor(private commonservice:CommonService,private route: Router,private http: HttpClient) { }

  ngOnInit(): void {
  this.commonservice.loggeduser.subscribe(user => {
   this.userDetails = user; 
  });  
  let  body={id:this.userDetails.id};
   
  }

}
