import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { CustomerLoginComponent } from './pages/customer-login/customer-login.component';
import { CustomerRegisterComponent } from './pages/customer-register/customer-register.component';
import { SellerRegisterComponent } from './pages/seller-register/seller-register.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
// import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { SellerDashboardComponent } from './pages/seller-dashboard/seller-dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { OuterlayoutComponent } from './outerlayout/outerlayout.component';
import { InnerlayoutComponent } from './innerlayout/innerlayout.component';
import { CommonService } from './service/common.service';
import { NumericinputDirective } from './directive/numericinput.directive';
import { FileUploaderModule } from "ng4-file-upload";
import { SellerProfileComponent } from './pages/seller-profile/seller-profile.component';
import { ShopSettingComponent } from './pages/shop-setting/shop-setting.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { FileUploadService } from './file-upload/file-upload.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { DataTablesModule } from "angular-datatables";
import { SellerOrderComponent } from './pages/seller-order/seller-order.component';
import { SellerOrderDetailComponent } from './pages/seller-order-detail/seller-order-detail.component';
import { SellerPurchaseHistoryComponent } from './pages/seller-purchase-history/seller-purchase-history.component';
import { SellerWishlistComponent } from './pages/seller-wishlist/seller-wishlist.component';
import { SellerProductReviewComponent } from './pages/seller-product-review/seller-product-review.component';
import { SellerPaymentHistoryComponent } from './pages/seller-payment-history/seller-payment-history.component';
import { SellerMoneyWithdrawComponent } from './pages/seller-money-withdraw/seller-money-withdraw.component';
import { SellerSupportTicketComponent } from './pages/seller-support-ticket/seller-support-ticket.component';
import { ProductUploadComponent } from './pages/product-upload/product-upload.component';
import { SellerProductListComponent } from './pages/seller-product-list/seller-product-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TagInputModule } from 'ngx-chips';
import { FileUploadMultipleComponent } from './file-upload-multiple/file-upload-multiple.component';
import { FileUploadMultipleService } from './file-upload-multiple/file-upload-multiple.service';
import {ProgressBarModule} from "angular-progress-bar";
import { DecimalinputDirective } from './directive/decimalinput.directive'
import { TreeviewModule } from 'ngx-treeview';
import { TreeDropdownDirective } from './directive/tree-dropdown.directive';
import { SellerdashboardComponent } from './sellerdashboard/sellerdashboard.component';
import { ProductEditComponent } from './pages/product-edit/product-edit.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SellerProductReviewDetailComponent } from './pages/seller-product-review-detail/seller-product-review-detail.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { CustomerInnerlayoutComponent } from './customer-innerlayout/customer-innerlayout.component';
import { CustomerSidebarComponent } from './customer-sidebar/customer-sidebar.component';
import { CustomerProfileComponent } from './pages/customer-profile/customer-profile.component';
import { CustomerPurchaseHistoryComponent } from './pages/customer-purchase-history/customer-purchase-history.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { CartComponent } from './pages/cart/cart.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component';
import { CustomerOrderDetailComponent } from './pages/customer-order-detail/customer-order-detail.component';
import { FilterPipe } from './pipe/filter.pipe'; // your pipe path 
import {NgxPaginationModule} from 'ngx-pagination';
import { AboutComponent } from './pages/cms/about/about.component';
import { ContactUsComponent } from './pages/cms/contact-us/contact-us.component';
import { BrandsComponent } from './pages/brands/brands.component';
import { BlogsComponent } from './pages/cms/blogs/blogs.component';
import { BlogDetailsComponent } from './pages/cms/blog-details/blog-details.component';
import { PrivacyPolicyComponent } from './pages/cms/privacy-policy/privacy-policy.component';
import { ShippingDeliveryComponent } from './pages/cms/shipping-delivery/shipping-delivery.component';
import { ReturnPolicyComponent } from './pages/cms/return-policy/return-policy.component';
import { PaymentOptionsComponent } from './pages/cms/payment-options/payment-options.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CustomerLoginComponent,
    CustomerRegisterComponent,
    SellerRegisterComponent,
    DashboardComponent,
    ProductListComponent,
    ProductDetailsComponent,
    SellerDashboardComponent,
    SidebarComponent,
    OuterlayoutComponent,
    InnerlayoutComponent,
    NumericinputDirective,
    SellerProfileComponent,
    ShopSettingComponent,
    FileUploadComponent,
    SellerOrderComponent,
    SellerOrderDetailComponent,
    SellerPurchaseHistoryComponent,
    SellerWishlistComponent,
    SellerProductReviewComponent,
    SellerPaymentHistoryComponent,
    SellerMoneyWithdrawComponent,
    SellerSupportTicketComponent,
    ProductUploadComponent,
    SellerProductListComponent,
    FileUploadMultipleComponent,
    DecimalinputDirective,
    TreeDropdownDirective,
    ProductEditComponent,
    SellerProductReviewDetailComponent,
    CustomerInnerlayoutComponent,
    CustomerSidebarComponent,
    CustomerProfileComponent,
    CustomerPurchaseHistoryComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    CartComponent,
    CheckoutComponent,
    PaymentSuccessComponent,
    CustomerOrderDetailComponent,
    FilterPipe,
    AboutComponent,
    ContactUsComponent,
    BrandsComponent,
    BlogsComponent,
    BlogDetailsComponent,
    PrivacyPolicyComponent,
    ShippingDeliveryComponent,
    ReturnPolicyComponent,
    PaymentOptionsComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,  BrowserAnimationsModule,ReactiveFormsModule,FileUploaderModule,
    ToastrModule.forRoot(),HttpClientModule,NgbModule,NgxFileDropModule,DataTablesModule,NgSelectModule,AngularEditorModule,TagInputModule,ProgressBarModule,TreeviewModule.forRoot(),InfiniteScrollModule,
    NgxSpinnerModule,CarouselModule,NgxImageZoomModule,NgxPaginationModule,NgxSliderModule
  ],
  providers: [CommonService,ConfirmationDialogService,FileUploadComponent,FileUploadService,FileUploadMultipleService
],
  bootstrap: [AppComponent]
})
export class AppModule { } 
