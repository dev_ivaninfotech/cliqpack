import { Component, OnInit } from '@angular/core';
import { NgbActiveModal,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../service/common.service';
import { NotificationService } from '../notification.service'

import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  active = 1;
  user_id:any;
  upload_type:any;
  rowSet:any = {};
  public files: NgxFileDropEntry[] = [];
  constructor(private activeModal: NgbActiveModal,private commonservice:CommonService,private notifyService: NotificationService) { }

  ngOnInit(): void {
      
  this.commonservice.loggeduser.subscribe(user => {
        this.user_id = user.id; 
        
       }); 
       
      
  }
 public decline() {
    this.activeModal.close(false);
  }

  public accept() {
    this.activeModal.close(true);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }
  
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
        let body={upload_file:file,user_id:this.user_id};
            this.commonservice.postData(body,"seller/uploadFile").subscribe(data => {
            if(data.status)
            {
               this.rowSet=data.data;
               this.rowSet.upload_type=this.commonservice.returnuploadType();
               
               this.commonservice.setfilePath(this.rowSet); 
               this.activeModal.dismiss();
               
            }
            else{
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "cliqPack")
                }
            }
            },
            err => {
              console.log("Error occured.")
            });  
          

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

}
