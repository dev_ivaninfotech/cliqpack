import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { FileUploadComponent } from './file-upload.component';


@Injectable()
export class FileUploadService {

  constructor(private modalService: NgbModal) { }

  public gallery(
    
    dialogSize: 'sm'|'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(FileUploadComponent, { size: dialogSize });
    return modalRef.result;
  }

}
