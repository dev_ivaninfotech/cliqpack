import { Component, OnInit,AfterViewInit } from '@angular/core';
import { NgbActiveModal,NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../service/common.service';
import { NotificationService } from '../notification.service'
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-file-upload-multiple',
  templateUrl: './file-upload-multiple.component.html',
  styleUrls: ['./file-upload-multiple.component.css']
})
export class FileUploadMultipleComponent implements OnInit {
active = 1;
  user_id:any;
  upload_type:any;
  rowSet:any = {};
  public files: NgxFileDropEntry[] = [];
  multiple:boolean;
  acceptType:any;
  loadType:any;
  loadTpl:boolean=false;
  service_url:any;
  showprogress:boolean;
  uploadpercent:any;
  cal_percent:any;
  galleryForm: FormGroup;
  sortOptions:any=[];
  galleryimages:any;
  curPage: number;
  pageSize: number;
  selectItems:any=[];

  constructor(private activeModal: NgbActiveModal,private commonservice:CommonService,private notifyService: NotificationService,private formBuilder: FormBuilder,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
      
  this.commonservice.loggeduser.subscribe(user => {
        this.user_id = user.id; 
        this.galleryForm = this.formBuilder.group({
            user_id:[this.user_id],
            file_original_name:[''],
            orderBy: ['newest'], 
            type:['']
          })
          this.sortOptions=[{text:'Sort by newest',value:'newest'},{text:'Sort by oldest',value:'oldest'},{text:'Sort by smallest',value:'smallest'},{text:'Sort by largest',value:'largest'}];
       }); 
    this.commonservice.uploadSetting.subscribe(data => {
        
        this.multiple = data.multiple; 
        this.acceptType = data.acceptType; 
        this.loadType = data.loadType;
        this.loadTpl=true; 
        this.service_url=data.service_url;
        this.uploadpercent=0;
       
        this.curPage = 1;
        this.pageSize = 12; // any page size you want 
        console.log(this.loadType);
        if(this.loadType=='gallery' || this.loadType=='thumb' || this.loadType=='meta_img' || this.loadType=='shoplogo' || this.loadType=='banner' || this.loadType=='avtar')
        {
         this.galleryForm.patchValue({
            type:'image',
          });  
           this.populateGallery();   
        }
        else{
          this.galleryForm.patchValue({
            type:'pdf',
          });  
           this.populateGallery();    
        }
       });         
      
  }
 public decline() {
    this.activeModal.close(false);
  }

  public accept() {
    this.activeModal.close(true);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }
  
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    let count=1;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        this.spinner.show();  
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
           
            this.cal_percent= (count/this.files.length)*100  
            this.uploadpercent=this.cal_percent;    
            let body={upload_file:file,user_id:this.user_id};
            this.commonservice.postData(body,"product/"+this.service_url).subscribe(data => {
             this.spinner.hide();
            if(data.status)
            {
                
               this.rowSet=data.data;
               this.rowSet.upload_type=this.loadType;
               
               this.commonservice.setfilePath(this.rowSet); 
              
               this.activeModal.dismiss();
               
            }
            else{
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Upload")
                }
            }
            },
            err => {
              console.log("Error occured.")
            });  
          

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
      count++;
    }
  }
  numberOfPages() {
   return Math.ceil(this.galleryimages.length / this.pageSize);
 };
 populateGallery()
 {
     this.spinner.show();
     this.commonservice.postData(this.galleryForm.value,"product/listGallery").subscribe(data => {
             
            if(data.status)
            {
              this.spinner.hide();  
              this.selectItems=[];  
              this.galleryimages=data.data;
               
            }
            else{
                this.spinner.hide();
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Upload")
                }
            }
            },
            err => {
              console.log("Error occured.")
            });  
 }
 selectPhoto(data_item)
 {
     if(this.multiple){
     if(this.selectItems.includes(data_item))
     {
         this.selectItems.splice(this.selectItems.indexOf(data_item), 1);
         
     }
     else{
         this.selectItems.push(data_item);
        
     }
     }
     else{
         this.selectItems[0]=data_item;
     }
 }
  selectPdf(data_item)
 {
     this.selectItems[0]=data_item;
     console.log(this.selectItems);
 }
 addItem():void{
 let loadType=this.loadType;
 let commonservice=this.commonservice;
 this.selectItems.forEach(function (value) {
 let rowSet=value;
 rowSet.upload_type=loadType;
 commonservice.setfilePath(rowSet); 

}); 
this.activeModal.dismiss();
 }
}
