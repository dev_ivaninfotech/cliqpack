import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadMultipleComponent } from './file-upload-multiple.component';
@Injectable({
  providedIn: 'root'
})
export class FileUploadMultipleService {
constructor(private modalService: NgbModal) { }
public gallery(
    
    dialogSize: 'sm'|'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(FileUploadMultipleComponent, { size: dialogSize });
    return modalRef.result;
  }
}
