import { TestBed } from '@angular/core/testing';

import { FileUploadMultipleService } from './file-upload-multiple.service';

describe('FileUploadMultipleService', () => {
  let service: FileUploadMultipleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileUploadMultipleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
