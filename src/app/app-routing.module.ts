import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth.guard';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CustomerLoginComponent } from './pages/customer-login/customer-login.component';
import { CustomerRegisterComponent } from './pages/customer-register/customer-register.component';
import { SellerRegisterComponent } from './pages/seller-register/seller-register.component';
import { SellerProfileComponent } from './pages/seller-profile/seller-profile.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { SellerDashboardComponent } from './pages/seller-dashboard/seller-dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { OuterlayoutComponent } from './outerlayout/outerlayout.component';
import { InnerlayoutComponent } from './innerlayout/innerlayout.component';
import { ShopSettingComponent } from './pages/shop-setting/shop-setting.component';
import { SellerOrderComponent } from './pages/seller-order/seller-order.component';
import { SellerOrderDetailComponent } from './pages/seller-order-detail/seller-order-detail.component';
import { SellerPurchaseHistoryComponent } from './pages/seller-purchase-history/seller-purchase-history.component';
import { SellerWishlistComponent } from './pages/seller-wishlist/seller-wishlist.component';
import { SellerProductReviewComponent } from './pages/seller-product-review/seller-product-review.component';
import { SellerPaymentHistoryComponent } from './pages/seller-payment-history/seller-payment-history.component';
import { SellerMoneyWithdrawComponent } from './pages/seller-money-withdraw/seller-money-withdraw.component';
import { SellerSupportTicketComponent } from './pages/seller-support-ticket/seller-support-ticket.component';
import { ProductUploadComponent } from './pages/product-upload/product-upload.component';
import { SellerProductListComponent } from './pages/seller-product-list/seller-product-list.component';
import { SellerdashboardComponent } from './sellerdashboard/sellerdashboard.component';
import { ProductEditComponent } from './pages/product-edit/product-edit.component';
import { SellerProductReviewDetailComponent } from './pages/seller-product-review-detail/seller-product-review-detail.component';
import { CustomerInnerlayoutComponent } from './customer-innerlayout/customer-innerlayout.component';
import { CustomerSidebarComponent } from './customer-sidebar/customer-sidebar.component';
import { CustomerProfileComponent } from './pages/customer-profile/customer-profile.component';
import { CustomerPurchaseHistoryComponent } from './pages/customer-purchase-history/customer-purchase-history.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { CartComponent } from './pages/cart/cart.component';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component';
import { CustomerOrderDetailComponent } from './pages/customer-order-detail/customer-order-detail.component';
import { AboutComponent } from './pages/cms/about/about.component';
import { ContactUsComponent } from './pages/cms/contact-us/contact-us.component';
import { BrandsComponent } from './pages/brands/brands.component';
import { BlogsComponent } from './pages/cms/blogs/blogs.component';
import { BlogDetailsComponent } from './pages/cms/blog-details/blog-details.component';
import { PrivacyPolicyComponent } from './pages/cms/privacy-policy/privacy-policy.component';
import { ShippingDeliveryComponent } from './pages/cms/shipping-delivery/shipping-delivery.component';
import { ReturnPolicyComponent } from './pages/cms/return-policy/return-policy.component';
import { PaymentOptionsComponent } from './pages/cms/payment-options/payment-options.component';

const routes: Routes = [
  {
    path: '',
    component: OuterlayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'login', component: CustomerLoginComponent },
      { path: 'register', component: CustomerRegisterComponent },
      { path: 'products/:search/:searchId', component: ProductListComponent },
      { path: 'product/:productId', component: ProductDetailsComponent },
      { path: 'seller-signup', component: SellerRegisterComponent },
      { path: 'test', component: SellerdashboardComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'reset-password/:id', component: ResetPasswordComponent },
      { path: 'cart', component: CartComponent, canActivate: [AuthGuard] },
      { path: 'payment-success', component: PaymentSuccessComponent },
      
      { path: 'brands', component: BrandsComponent },
      { path: 'about-us', component: AboutComponent },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'blogs', component: BlogsComponent },
      { path: 'blog-categories/:categoryId/:categorySlug', component: BlogsComponent },
      { path: 'blog-details/:blogId/:blogSlug', component: BlogDetailsComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent },
      { path: 'shipping-delivery', component: ShippingDeliveryComponent },
      { path: 'return-policy', component: ReturnPolicyComponent },
      { path: 'payment-options', component: PaymentOptionsComponent },
    ]
  },
  {
    path: '',
    component: CustomerInnerlayoutComponent,
    children: [
      {
        path: 'customer/dashboard', canActivate: [AuthGuard], component: DashboardComponent,
      },
      {
        path: 'customer/profile', canActivate: [AuthGuard], component: CustomerProfileComponent,
      },
      {
        path: 'customer/purchase-history', canActivate: [AuthGuard], component: CustomerPurchaseHistoryComponent,
      },
      {
        path: 'customer/wishlist', canActivate: [AuthGuard], component: SellerWishlistComponent,
      },
      {
        path: 'customer/support-ticket', canActivate: [AuthGuard], component: SellerSupportTicketComponent,
      },
      {
        path: 'customer/vieworder/:id', canActivate: [AuthGuard], component: CustomerOrderDetailComponent
      },

    ]
  },
  {
    path: '',
    component: InnerlayoutComponent,
    children: [

      {
        path: 'seller/dashboard', canActivate: [AuthGuard], component: SellerDashboardComponent
      },
      {
        path: 'seller/profile', canActivate: [AuthGuard], component: SellerProfileComponent
      },
      {
        path: 'seller/shop', canActivate: [AuthGuard], component: ShopSettingComponent
      },
      {
        path: 'seller/order', canActivate: [AuthGuard], component: SellerOrderComponent
      },
      {
        path: 'seller/vieworder/:id', canActivate: [AuthGuard], component: SellerOrderDetailComponent
      },

      {
        path: 'seller/purchase-history', canActivate: [AuthGuard], component: SellerPurchaseHistoryComponent
      },
      {
        path: 'seller/wishlist', canActivate: [AuthGuard], component: SellerWishlistComponent
      },
      {
        path: 'seller/product-review', canActivate: [AuthGuard], component: SellerProductReviewComponent
      },
      {
        path: 'seller/payment-history', canActivate: [AuthGuard], component: SellerPaymentHistoryComponent
      },
      {
        path: 'seller/money-withdraw', canActivate: [AuthGuard], component: SellerMoneyWithdrawComponent
      },
      {
        path: 'seller/support-ticket', canActivate: [AuthGuard], component: SellerSupportTicketComponent
      },
      {
        path: 'seller/product-upload', canActivate: [AuthGuard], component: ProductUploadComponent
      },
      {
        path: 'seller/product-listing', canActivate: [AuthGuard], component: SellerProductListComponent
      },
      {
        path: 'seller/product-edit/:id', canActivate: [AuthGuard], component: ProductEditComponent
      },
      {
        path: 'seller/product-review-detail/:id', canActivate: [AuthGuard], component: SellerProductReviewDetailComponent
      },
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { 
      // useHash: true ,      
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
