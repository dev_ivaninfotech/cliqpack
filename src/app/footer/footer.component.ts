import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from '../notification.service';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  subscribeForm: FormGroup;
  contactAddres: any = {
    address: null,
    city: null,
    email: null,
    phone: null,
  };

  constructor(
    public formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private http: HttpClient,
		private notifyService: NotificationService,
		private api: ServicesService,
  ) {
    this.subscribeForm = formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
    });
  }

  ngOnInit(): void {
    this.http.post<any>(this.api.api_url + 'siteAddress', {})
			.subscribe((res: any) => {
				if (res.status == true) {
					this.contactAddres = res.data;
				} else {
					this.notifyService.showError(res.message, 'Error Message');
				}
			}, (error: any) => {
				this.notifyService.showError("Somthing went wrong !!!!", "Error Message");
			});
  }

  subscribeSubmit(event: any){
    if (this.subscribeForm.valid) {
      this.spinner.show();

      this.http.post<any>(this.api.api_url + 'subscribe', { 'email': this.subscribeForm.value.email })
			.subscribe((res: any) => {
				if (res.status == true) {
					this.subscribeForm.reset();
					this.notifyService.showSuccess(res.message, 'Success Message');
				} else {
					this.notifyService.showError(res.message, 'Error Message');
				}
        this.spinner.hide();
			}, (error: any) => {
				this.notifyService.showError("Somthing went wrong !!!!", "Error Message");
        this.spinner.hide();
			});
    }
  }
}
