import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerInnerlayoutComponent } from './customer-innerlayout.component';

describe('CustomerInnerlayoutComponent', () => {
  let component: CustomerInnerlayoutComponent;
  let fixture: ComponentFixture<CustomerInnerlayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerInnerlayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInnerlayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
