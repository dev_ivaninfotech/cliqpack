import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common.service';

@Component({
  selector: 'app-sellerdashboard',
  templateUrl: './sellerdashboard.component.html',
  styleUrls: ['./sellerdashboard.component.css']
})
export class SellerdashboardComponent implements OnInit {
  categories:any;
  constructor(private commonservice:CommonService) { }

  ngOnInit(): void {
  this.commonservice.postData('',"categories/treeView").subscribe(data => {
    if(data.status)
    {
      this.categories=data.data;
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });      
      
      
      
  }

}
