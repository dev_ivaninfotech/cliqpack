import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'cliqpack';

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (!(event instanceof NavigationEnd)) {
        return;
      }
      
      $('html,body').animate({ scrollTop: 0 }, 'slow');
    });
  }
}

// showToasterSuccess(){
//   this.notifyService.showSuccess("Data shown successfully !!", "ItSolutionStuff.com")
// }

// showToasterError(){
//   this.notifyService.showError("Something is wrong", "ItSolutionStuff.com")
// }

// showToasterInfo(){
//   this.notifyService.showInfo("This is info", "ItSolutionStuff.com")
// }

// showToasterWarning(){
//   this.notifyService.showWarning("This is warning", "ItSolutionStuff.com")
// }
