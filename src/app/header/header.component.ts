import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ServicesService } from '../services.service';
import { NotificationService } from '../notification.service'
import { AuthService } from '../auth.service'
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from '../service/common.service';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userDetails: any;
  menues: any;
  searchForm: FormGroup;
  total_cart_item:any;
  device_token:any;
  params_body:any
  @ViewChildren('dd') dds: QueryList<NgbDropdown>;
  constructor(private route: Router, private notifyService: NotificationService, private http: HttpClient, private api: ServicesService, private auth: AuthService, private commonservice: CommonService, public formBuilder: FormBuilder,) {
    this.searchForm = formBuilder.group({
      'keyword': [null],
    });
  }
  
  public log_val: any;
  public log_url: string;

  ngOnInit(): void {
      this.totalcartItem();  
    if (localStorage.getItem('isLoggedIn') === 'true') {
      this.log_val = true;

    }
    else {
      this.log_val = false;

      // this.route.navigate(['/']);
    }
    this.commonservice.loggeduser.subscribe(user => {
      this.userDetails = user;
    });

    this.commonservice.postData('', "categories/treeView").subscribe(data => {
      if (data.status) {
        this.menues = data.data;
      }
    },
      err => {
        console.log("Error occured.", err)
      });
this.commonservice.totalcartItem.subscribe(cartItem => {
  this.totalcartItem(); 
  }); 

  }

  logOut() {


    const auth_token = window.localStorage.getItem('authtoken');
    const api_url = this.api.api_url;
    const url = api_url + 'auth/web/logout';
    // console.log(auth_token);
    let headers = new HttpHeaders({
      'content-type': 'application/json',
      'Authorization': auth_token
    })
    // headers.append('content-type', 'application/json')
    // headers.append('Access-Control-Allow-Origin', '*')
    // headers.append('Authorization', auth_token)

    // let options = { headers: headers };

    // const options :any={
    //   'content-type': 'application/json',
    //   'Authorization':auth_token
    // };

    // console.log(options);
    const formData = '';

    this.http.post<any>(url, formData, { headers: headers }).subscribe((res: any) => {
      console.log(res);
      if (res.status == true) {
        this.auth.logout();
         window.localStorage.clear(); 
        this.route.navigate(['/login'])
          .then(() => {
            window.location.reload();
          });
        //this.route.navigateByUrl('/login');
      } else {
        this.notifyService.showWarning("Somthing went wrong !!!!", "")
      }
    },

      (err: any) => {
        // this.notifyService.showWarning("Somthing went wrong !!!!", "")
      }

    );

  }

  actionClick() {
    this.dds.forEach((dd) => {
      dd.close();
    })
  }

  searchSubmit() {
    this.route.navigate(['/products/search/main'], { queryParams: this.searchForm.value })
  }

  totalcartItem()
  {
        this.device_token=window.localStorage.getItem("device_token") 
         this.params_body={user_id:'',device_token:''};
        if(this.device_token!=null || this.userDetails!=null)
        {
        if(this.device_token!=null && this.userDetails==null)
        {
           
           this.params_body.device_token=this.device_token;  
        }
        if(this.userDetails!=null)
        {
        this.params_body.user_id=this.userDetails.id; 
        }   
        this.commonservice.postData(this.params_body,"carts/totalcartItrm").subscribe(resp => {
        if(resp.status)
         {
            this.total_cart_item=resp.data;
         }
         },
        err => {
          console.log("Error occured.",err)
        });
        }
        else{
            this.total_cart_item=0;
        }
                  
  } 
}