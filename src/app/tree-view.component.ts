import {Component, Input} from '@angular/core';

@Component ({
  selector: 'tree-view',
  template: `
  <ul>
    <li *ngFor="node of treeData">
      {{node.text}}
      <tree-view [treeData]="node.children"></tree-view>
    </li>
</ul>
  `
})
export class TreeView {
  @Input() treeData: [];
}