import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProductReviewDetailComponent } from './seller-product-review-detail.component';

describe('SellerProductReviewDetailComponent', () => {
  let component: SellerProductReviewDetailComponent;
  let fixture: ComponentFixture<SellerProductReviewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProductReviewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProductReviewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
