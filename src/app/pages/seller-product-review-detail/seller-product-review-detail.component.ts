import { Component, OnInit,AfterViewInit,ViewChild  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-seller-product-review-detail',
  templateUrl: './seller-product-review-detail.component.html',
  styleUrls: ['./seller-product-review-detail.component.css']
})
export class SellerProductReviewDetailComponent implements OnInit {
  userDetails:any;
  reviews:any;
  maxRating:any;
  ratingReadonly:boolean=true
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private confirmationDialogService: ConfirmationDialogService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  this.maxRating=5;    
  this.commonservice.loggeduser.subscribe(user => {
  let body={id:atob(this.activatedRoute.snapshot.params.id)};    
  this.commonservice.postData(body,"seller/reviewDetail").subscribe(resp => {
       if(resp.status)
        {
          this.reviews=resp.data;
          
        }

        },
        err => {
          console.log("Error occured.",err)
        });        
  });    
  }    
      
  

}
