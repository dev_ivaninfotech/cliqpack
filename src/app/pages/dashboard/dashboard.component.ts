import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
   userDetails:any;
   total_order:any;
   total_wishlists:any;
   setting:any;
   customer_package:any;
   address:any;
   order_lebel:any;
   wishlist_lebel:any;
   total_cart_order:any=0;
   show_renew:boolean
  constructor(private commonservice:CommonService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  this.spinner.show();    
  this.commonservice.loggeduser.subscribe(user => {
  this.userDetails = user; 
  let body={id:this.userDetails.id};
   this.commonservice.postData(body,"customer/dashboard").subscribe(data => {
     this.spinner.hide();   
    if(data.status)
    {
      this.total_order=data.data.total_order;
      this.total_wishlists=data.data.total_wishlists;
      this.setting=data.data.setting;
      this.customer_package=data.data.customer_package;
      this.address=data.data.address;
      this.order_lebel=this.total_order>0?'Product(s)':'Product';
      this.wishlist_lebel=this.total_wishlists>0?'Product(s)':'Product';
      this.show_renew=this.setting.value;
      console.log(this.setting);
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });   
        
});          
  }

}
