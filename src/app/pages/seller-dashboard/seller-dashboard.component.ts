import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-seller-dashboard',
  templateUrl: './seller-dashboard.component.html',
  styleUrls: ['./seller-dashboard.component.css']
})
export class SellerDashboardComponent implements OnInit {
  userDetails:any;
  sellerDetails:any;
  constructor(private commonservice:CommonService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  this.spinner.show();    
  this.commonservice.loggeduser.subscribe(user => {
  this.userDetails = user; 
  let body={id:this.userDetails.id};
   this.commonservice.postData(body,"seller/dashboard").subscribe(data => {
    if(data.status)
    {
      this.sellerDetails=data.data;
      this.spinner.hide();
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });   
        
});      
      
      
      
      
  }

}
