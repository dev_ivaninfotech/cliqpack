import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-seller-wishlist',
  templateUrl: './seller-wishlist.component.html',
  styleUrls: ['./seller-wishlist.component.css']
})
export class SellerWishlistComponent implements OnInit {
  rows:any = [];
  start:any = 0;
  throttle:any = 300;
  scrollDistance:any = 1;
  scrollUpDistance:any = 2;
  direction:any = "";
  userDetails:any;
  maxRating:any=5;
  ratingReadonly:boolean=true
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private activatedRoute: ActivatedRoute,private confirmationDialogService: ConfirmationDialogService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  this.commonservice.loggeduser.subscribe(user => {
  this.userDetails= user;   
  let body={id:this.userDetails.id,start:this.start};    
  this.spinner.show();
  this.commonservice.postData(body,"wishlists/index_web").subscribe(resp => {
    this.spinner.hide();  
    if(resp.status)
     {
       this.rows=resp.data;
       

     }

     },
     err => {
       console.log("Error occured.",err)
     });        
  });    
  }
  
onScrollDown(ev) {
   

    // add another 20 items
    this.start=parseInt(this.start)+15;
    let body={id:this.userDetails.id,start:this.start};    
    this.commonservice.postData(body,"wishlists/index_web").subscribe(resp => {
    if(resp.status)
     {
       
        let wishlists=resp.data;
        if(wishlists.length>0) {
        wishlists.forEach(item=>{
        this.rows.push(item);    
        })
        }

     }

     },
     err => {
       console.log("Error occured.",err)
     });        
    this.direction = "down";
  }

  onUp(ev) {
    this.start=parseInt(this.start)+15;
    let body={id:this.userDetails.id,start:this.start};    
    this.commonservice.postData(body,"wishlists/index_web").subscribe(resp => {
    if(resp.status)
     {
       
        let wishlists=resp.data;
        if(wishlists.length>0) {
        wishlists.forEach(item=>{
        this.rows.unshift(item);    
        })
        }

     }

     },
     err => {
       console.log("Error occured.",err)
     });        
    
    this.direction = "up";
  }
  
  removeFromWishlist(i,id)
  {
      this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove this item from wishlist... ?')
    .then((confirmed) => {
     if(confirmed) {  
       let body={id:id};
      this.commonservice.postData(body,"wishlists/deleteWishlist").subscribe(resp => {
      if(resp.status)
      {
       this.rows.splice(i, 1);   
       this.notifyService.showError("This Item has been reoved form your wishlist", "Wishlist")

      }
     },
     err => {
       console.log("Error occured.",err)
     });        
     
     }    
     })
   
     .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));           
      
  }
  
  
  
}
