import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerWishlistComponent } from './seller-wishlist.component';

describe('SellerWishlistComponent', () => {
  let component: SellerWishlistComponent;
  let fixture: ComponentFixture<SellerWishlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerWishlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerWishlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
