import { Component, OnInit,AfterViewInit, ViewChild, ViewChildren, QueryList  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject,BehaviorSubject } from "rxjs";
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {ModalDismissReasons, NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

class searchFilter {
  name: "";
}
@Component({
  selector: 'app-seller-money-withdraw',
  templateUrl: './seller-money-withdraw.component.html',
  styleUrls: ['./seller-money-withdraw.component.css']
})
export class SellerMoneyWithdrawComponent implements OnInit {
   userDetails:any;
   rows:any;
   rows_approved:any;
   start_index:any;
   start_index_approve:any;
   start_index_withdrawable:any;
   start_index_lock:any;
   netlockedamnt:any;
   netwithdrawableamnt:any;
   netapprovedamnt:any;
   netpendingamnt:any;
   max_request_amnt:any
   withdrawForm: FormGroup;
   IsSubmitted: boolean;
   isDisabled:boolean=false;
   loading:boolean = false;
   dtOptions: DataTables.Settings = {};
   dtOptions2: DataTables.Settings = {};
   dtOptions3: DataTables.Settings = {};
   dtOptions4: DataTables.Settings = {};

   active:any;
   searchForm: searchFilter = new searchFilter();
   approved_amnt:any;
   seller_msg:any;
   admin_msg:any;
   transaction_date:any;
   payement_method:any;
   rows_withdrawables:any;
   rows_lock:any;
   @ViewChildren(DataTableDirective) 
   datatableElementList: QueryList<DataTableDirective>;
    public dtTrigger1 = new BehaviorSubject <any>('');
    public dtTrigger2 = new BehaviorSubject <any>('');
    public dtTrigger3 = new BehaviorSubject <any>('');
    public dtTrigger4 = new BehaviorSubject <any>('');
   
   
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private confirmationDialogService: ConfirmationDialogService,
              private formBuilder: FormBuilder,private modalService: NgbModal,private spinner: NgxSpinnerService) { }

 ngOnInit(): void {
  this.active=1;   
  const that = this;
     this.commonservice.loggeduser.subscribe(user => {
     this.userDetails = user; 
       let body={seller_id:this.userDetails.seller_id,id:this.userDetails.id};
       this.commonservice.postData(body,"seller/netFigure").subscribe(resp => {
       if(resp.status)
        {
          this.netlockedamnt=resp.netlockedamnt;
          this.netwithdrawableamnt=resp.netwithdrawableamnt;
          this.netapprovedamnt=resp.netapprovedamnt;
          this.netpendingamnt=resp.netpendingamnt;
          this.max_request_amnt=resp.max_request_amnt
          this.withdrawForm = this.formBuilder.group({
            user_id:[this.userDetails.seller_id,Validators.required],
            amount:['',[Validators.required,Validators.max(this.max_request_amnt)]],
            message: [''],  
          })
          
        }

        },
        err => {
          console.log("Error occured.",err)
        });       
     this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      destroy:true,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.seller_id= this.userDetails.seller_id; 
       dataTablesParameters.id= this.userDetails.id;  
       dataTablesParameters.admin_to_pay= this.userDetails.admin_to_pay;  
       dataTablesParameters.status= 0;  

       this.commonservice.postData(dataTablesParameters,"seller/withdraw-request").subscribe(resp => {
       if(resp.status)
        {
          this.rows=resp.data;
          this.start_index=resp.start_index;
          
          
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      //columns: [{ data: 'sl' }, { data: 'name' }, { data: 'category' },{ data: 'qty' },{ data: 'stock' },{ data: 'unit_price' },{data: 'published'},{data: 'featured'},{data: 'action'}]
    }; 
    
    this.dtOptions2 = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      destroy:true,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.seller_id= this.userDetails.seller_id; 
       dataTablesParameters.id= this.userDetails.id; 
       dataTablesParameters.admin_to_pay= this.userDetails.admin_to_pay;  
       dataTablesParameters.status=1;  
       this.commonservice.postData(dataTablesParameters,"seller/withdraw-request").subscribe(resp => {
       if(resp.status)
        {
          this.rows_approved=resp.data;
          this.start_index_approve=resp.start_index;
         
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      //columns: [{ data: 'sl' }, { data: 'name' }, { data: 'category' },{ data: 'qty' },{ data: 'stock' },{ data: 'unit_price' },{data: 'published'},{data: 'featured'},{data: 'action'}]
    };     
    
    this.dtOptions3 = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      destroy:true,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.id= this.userDetails.id; 
       this.commonservice.postData(dataTablesParameters,"seller/pendingamntList").subscribe(resp => {
       if(resp.status)
        {
          this.rows_withdrawables=resp.data;
          this.start_index_withdrawable=resp.start_index;
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      //columns: [{ data: 'sl' }, { data: 'name' }, { data: 'category' },{ data: 'qty' },{ data: 'stock' },{ data: 'unit_price' },{data: 'published'},{data: 'featured'},{data: 'action'}]
    }; 
    
    this.dtOptions4 = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      destroy:true,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.id= this.userDetails.id; 
       this.commonservice.postData(dataTablesParameters,"seller/lockedamntList").subscribe(resp => {
       if(resp.status)
        {
          this.rows_lock=resp.data;
          this.start_index_lock=resp.start_index;
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      //columns: [{ data: 'sl' }, { data: 'name' }, { data: 'category' },{ data: 'qty' },{ data: 'stock' },{ data: 'unit_price' },{data: 'published'},{data: 'featured'},{data: 'action'}]
    };     
    
        
        
   });        
  }
ngAfterViewInit(): void {
    this.datatableElementList.forEach(datatableElement=>{
    this.dtTrigger1.subscribe(() => {
      datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $("input", this.footer()).on("keyup change", function () {
            if (that.search() !== this["value"]) {
              that.search(this["value"]).draw();
            }
          });
        });
      });
    });
    this.dtTrigger2.subscribe(() => {
      datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $("input", this.footer()).on("keyup change", function () {
            if (that.search() !== this["value"]) {
              that.search(this["value"]).draw();
            }
          });
        });
      });
    });
    this.dtTrigger3.subscribe(() => {
      datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $("input", this.footer()).on("keyup change", function () {
            if (that.search() !== this["value"]) {
              that.search(this["value"]).draw();
            }
          });
        });
      });
    });
    this.dtTrigger4.subscribe(() => {
      datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.columns().every(function () {
          const that = this;
          $("input", this.footer()).on("keyup change", function () {
            if (that.search() !== this["value"]) {
              that.search(this["value"]).draw();
            }
          });
        });
      });
    });
  });
  }
requestWithdraw(content) :void
{
    this.loading=false;
     if(this.max_request_amnt>0)
     {
     this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',size:'lg'}).result.then((res) => {
      
    }, (res) => {
      this.withdrawForm.patchValue({
            amount:'',
            message:'',  
           
          });   
      this.IsSubmitted=false;
    });  
    } 
    else{
        this.notifyService.showError("No more amount for requesting", "Money Withdraw")
    }
}

onSubmit():void
{
    this.spinner.show();
    this.IsSubmitted=true;
    if(this.withdrawForm.status=="VALID" )
 {
      this.isDisabled=true
      this.loading=true;
      this.commonservice.postData(this.withdrawForm.value,"seller/makeWithdraw").subscribe(data => {
          
      if(data.status)
        {
            
           this.isDisabled=false;
           this.IsSubmitted=false;
           this.dtTrigger1.next(this.withdrawForm.value);
           this.max_request_amnt=this.max_request_amnt-this.withdrawForm.get('amount').value;
           this.netpendingamnt=parseFloat(this.netpendingamnt)+parseFloat(this.withdrawForm.get('amount').value);
           this.netwithdrawableamnt=parseFloat(this.netwithdrawableamnt)-parseFloat(this.netpendingamnt);

           if(isNaN(this.netpendingamnt))
           {
               this.netpendingamnt=0;
           }
          this.loading=false;

           this.modalService.dismissAll();
          this.notifyService.showSuccess(data.message, "Money Withdraw")
          this.withdrawForm.patchValue({
            amount:'',
            message:'',  
           
          });   
          this.spinner.hide();
        }
        else{
                this.spinner.hide(); 
                this.isDisabled=false;
                this.loading=false;
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Profile")
                }
        }
        },
        err => {
          console.log("Error occured.")
        });  
     
 }    
    
}
withdrawDetails(content,item)
{
     this.seller_msg=item.message;
     this.admin_msg=item.admin_message;
     this.transaction_date=item.payment.created_at;
     this.payement_method=item.payment.payment_method; 
     this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',size:'lg'}).result.then((res) => {
     
     
    }, (res) => {
     
    });   
}
reInitial()
{
   
    setTimeout(()=>{                           //<<<---using ()=> syntax
       this.dtTrigger1 = new BehaviorSubject <any>('');
       this.dtTrigger2 = new BehaviorSubject <any>('');
       this.dtTrigger3 = new BehaviorSubject <any>('');
       this.dtTrigger4 = new BehaviorSubject <any>('');
 }, 1000);
}

onPaste(event: ClipboardEvent) {
    let clipboardData = event.clipboardData;
    let regexp = new RegExp('[^-_().;:,!a-zA-Z0-9/ ]');
    let pastedText = clipboardData.getData('text');
    let test = regexp.test(pastedText);
    let trimmedText = pastedText.replace(/[^a-zA-Z0-9-()._ ]/g, '');
    (<HTMLInputElement>document.getElementById('amount')).value = trimmedText;
    if (test) {
        event.preventDefault();
    }
}


}
