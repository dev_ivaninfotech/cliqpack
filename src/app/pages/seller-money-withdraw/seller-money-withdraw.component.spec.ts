import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerMoneyWithdrawComponent } from './seller-money-withdraw.component';

describe('SellerMoneyWithdrawComponent', () => {
  let component: SellerMoneyWithdrawComponent;
  let fixture: ComponentFixture<SellerMoneyWithdrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerMoneyWithdrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerMoneyWithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
