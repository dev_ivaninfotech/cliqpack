import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl,FormBuilder,Validators,FormArray } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router,ActivatedRoute  } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from '../../service/common.service';
import { OwlOptions } from 'ngx-owl-carousel-o';

export class Attribute {
        name:string;
        options: [];
        select_attribute:string
    }
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private route: Router,private acroute: ActivatedRoute, private notifyService: NotificationService, private http: HttpClient, 
  private api: ServicesService,private spinner: NgxSpinnerService,private commonservice:CommonService,private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute) { }
  public product :any;
  public sellerinfo :any;
  public related :any;
  maxRating:any=5;
  ratingReadonly:boolean=true;
  min_qty:any;
  variants:any=[];
  colors:any=[];
  variantForm: any = FormGroup;
  color:'';
  parents:any;
  cat_level:any;
  attributes:Attribute[]=[];
  combination:any='';
  customOptions: OwlOptions;
  customOptionsthumb: OwlOptions;
  customOptionssimilar: OwlOptions;
  device_token:any;
  userDetails:any;
  user_id:any="";
  start:any=0;
  products:any;
  searchForm: any = FormGroup;
  direction:any=""
  reviewForm: any = FormGroup;
  IsSubmitted:boolean=false;
  givenRating:any=''
  specifications:any;
  sub_specifications:any;
  ngOnInit(): void {
    this.commonservice.loggeduser.subscribe(user => {
		this.userDetails = user; 
                
	},err => {
		console.log("Error occured.",err)
	}); 
    if(this.userDetails!=null)
    {
        this.user_id=this.userDetails.id;
    }        
   this.device_token=window.localStorage.getItem("device_token") 
   if(this.device_token==null)
   {
      window.localStorage.setItem("device_token",Math.random().toString(36).substr(2, 9)) 
   }
   this.customOptions = {
    items: 6, 
    animateOut: 'slideOutUp',
    autoplay: false,
    nav: true,
    dots:false,
    loop: true,
  } 
   this.customOptionsthumb = {
    items: 1,   
    autoplay: false,
    nav: true,
    dots:false,
    loop: true,
  } 
   this.customOptionssimilar = {
       loop: true,
        margin: 30,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        dotsEach: false,
        //autoplay: true,
        nav: true,
        dots: false,
        navText: ['', ''],
        responsive: {
                0: {
                        items: 1
                },
                400: {
                        items: 2
                },
                740: {
                        items: 3
                },
                940: {
                        items: 6
                }
        },
    } 
    this.activatedRoute.params.subscribe(routeParams => {
    this.givenRating='';     
    let body={slug:routeParams.productId,user_id:this.user_id};
    this.spinner.show();
    this.attributes=[];  
    this.colors=[]; 
    this.commonservice.postData(body,"web/products/detailSlug").subscribe(data => {
    this.spinner.hide();   
    if(data.status)
    {
      
      this.product=data.data;
      this.sellerinfo=data.data.sellerinfo;
      this.min_qty=this.product.min_qty;
      this.combination=this.product.combination;
      this.specifications=this.product.specifications_new;
      this.sub_specifications=this.product.sub_specifications_new;
      this.product.attribute_values.forEach(item=>{
      let attritem={name:item.attr_name,options:item.attr_value,select_attribute:item.attr_value[0]};
      this.attributes.push(attritem);
    });
     if(this.userDetails !=null)
    {    
    this.reviewForm = this.formBuilder.group({
      name: [this.userDetails.name, Validators.required],
      email: [this.userDetails.email, [Validators.required]],
      rating: ['', [Validators.required]],
      comment: ['', [Validators.required]],
      user_id: [this.userDetails.id, [Validators.required]],
      product_id:[this.product.id, [Validators.required]]
    });
    }      
    this.start=0;
    this.searchForm = this.formBuilder.group({
        product_id:this.product.id,
        category_id:this.product.sub_category_id,
        start:this.start

        }); 
        this.listProductsInit();
        this.colors = this.product.colors; 
        this.color=this.colors[0].name;
        let body={name:this.product.sub_category_name};
        this.commonservice.postData(body,"categories/detailCategory").subscribe(resp => {
        if(resp.status)
         {
            this.parents=resp.data.parents;
            this.cat_level=parseInt(this.parents.length)-1;
         }
         },
         err => {
           console.log("Error occured.",err)
         });       
         
         
         //console.log(this.attributes);
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
         	
});
     
    
   
  }
  changeQty(increment)
  {
     if(increment)
     {
       this.min_qty=parseInt(this.min_qty)+1;  
     } 
     else{
         this.min_qty=parseInt(this.min_qty)-1;
     }
  }
  variantPrice()
  {
     
      let tmpcombination=[];
      
      if(this.color!='')
      {
          tmpcombination.push(this.color);
      }
      this.attributes.forEach(item=>{
       tmpcombination.push(item.select_attribute);
      });
      this.combination=tmpcombination.join('-');
      let body={combination:this.combination,id:this.product.id};
      this.commonservice.postData(body,"products/getPrice").subscribe(resp => {
        if(resp.status)
         {
          this.product.discount_base_price=resp.netPrice; 
          this.product.base_price=resp.mrpPrice;
          this.product.availableQty=resp.availableQty
          if(resp.mrpPrice!=resp.netPrice)
          {
              this.product.show_actual=1;
          }
          else{
              this.product.show_actual=0;
          }
          
          
         }
         },
         err => {
           console.log("Error occured.",err)
         });       
      
  }
  
  
  
  setColor(color)
  {
      this.color=color;
      this.variantPrice();
  }
  setVariation(i,attr)
  {
      this.attributes[i].select_attribute=attr;
      this.variantPrice();
  }
  addCart()
  {
     this.spinner.show();  
     let body={product_id:this.product.id,color:this.color,combination:this.combination,user_id:'',device_token:''};
 
     if(this.userDetails!=null) 
     {
          this.user_id=this.userDetails.id;
          body.user_id=this.user_id
     }
     else{
         this.user_id="";
          this.device_token=window.localStorage.getItem("device_token") ;
         body.device_token=this.device_token
     }
    
     
     
      this.commonservice.postData(body,"carts/addcartWeb").subscribe(resp => {
      this.spinner.hide();     
      if(resp.status)
        {
        
          this.commonservice.totalcartItem.next(resp.totalcartItem);
          this.notifyService.showSuccess("Item Added Successfully","Cart");

        }
        },
        err => {
          console.log("Error occured.",err)
        });       
     
      
  }
  listProductsInit():void{
    
    
    this.commonservice.postData(this.searchForm.value,"products/similarProducts").subscribe(resp => {  
    if(resp.status)
     {
        this.products=resp.data;
     }
     },
     err => {
       console.log("Error occured.",err)
     });       
  }
  
  addToWishList(item){
	if(this.userDetails==null)
        {
             this.notifyService.showError("Please login first", "Wishlist")
             this.route.navigate(['/login']);  
        }
        else{
        let body={user_id:this.userDetails.id,product_id:item.id};    
        this.commonservice.postData(body,"wishlists/store").subscribe(resp => {
        if(resp.status)
         {
            item.is_wishlist=!item.is_wishlist;
         }
         },
     err => {
       console.log("Error occured.",err)
     });           
	
        }
	 
}
onSubmit()
{
    this.IsSubmitted=true;
    this.reviewForm.patchValue({
    rating: this.givenRating,
  }); 
  if(this.reviewForm.status=="VALID")
  {
       this.spinner.show();
       this.commonservice.postData(this.reviewForm.value,"reviews/product/add").subscribe(resp => {
        this.spinner.hide();    
        if(resp.status)
         {

            this.givenRating="";
            this.reviewForm.reset(); 
            this.IsSubmitted=false;
            this.product.commentable=false;
            let userRow={reviewer_name:this.userDetails.name,profile_photo:this.userDetails.profile_img}
            let reviewRow={rating:resp.data.rating,comment:resp.data.comment,created_at:resp.data.created_at,user:userRow};
            this.product.reviews.unshift(reviewRow);
            this.commonservice.postData({product_id:this.product.id},"reviews/total").subscribe(resp => {
             this.spinner.hide();     
            if(resp.status)
             {
                this.product.avg_rating=resp.data.total_rating.rate;
                this.product.totalReview=resp.data.rating_count;
                this.notifyService.showSuccess("Your Review has been submitted successfully", "Review")
             }
             },
         err => {
           console.log("Error occured.",err)
         });        
            
            
            

         }

         },
         err => {
           console.log("Error occured.",err)
         });   
  }
   
}
}
