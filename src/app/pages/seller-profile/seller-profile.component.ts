import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { FileUploadMultipleService } from '../../file-upload-multiple/file-upload-multiple.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import {ModalDismissReasons, NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-seller-profile',
  templateUrl: './seller-profile.component.html',
  styleUrls: ['./seller-profile.component.css']
})
export class SellerProfileComponent implements OnInit {
  profileForm: FormGroup;
  emailForm: FormGroup;
  addressForm: FormGroup;
  IsSubmitted: boolean;
  IsadSubmit: boolean;
  logo_preview_url:any;
  thanas:any;
  postalcodes:any;
  areas:any;
  cities:any;
  userDetails:any;
  useraddresses:any;
  IsemailSubmit:any;  
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService, private http: HttpClient, 
            private formBuilder: FormBuilder,private modalService: NgbModal,
            private confirmationDialogService: ConfirmationDialogService,
             private fileuploadservice: FileUploadMultipleService,private spinner: NgxSpinnerService
  ) { }
  ngOnInit(): void {
       this.commonservice.loggeduser.subscribe(user => {
        this.userDetails = user; 
        this.addressForm = this.formBuilder.group({
            user_id:[this.userDetails.id,Validators.required],
            address:['',Validators.required],
            city: [null,Validators.required],
            thana:[null,Validators.required],
            postal_code:[null,Validators.required],
            area:[null,Validators.required],
            phone:['',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]],
            
          })
        this.profileForm = this.formBuilder.group({
            id: ['', Validators.required],
            name: ['', Validators.required],
            phone: ['',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.minLength(6)]],
            confirm_password: ['', [Validators.minLength(6)]],
            cash_on_delivery_status: [''],
            bank_payment_status: [''],
            bank_name: [''],
            bank_acc_name: [''],
            bank_acc_no: [''],
            bank_routing_no: [''],
          },
          { 
            validator: ConfirmedValidator('password', 'confirm_password')
          }

          ); 
          this.profileForm.patchValue(this.userDetails) ;
          this.emailForm = this.formBuilder.group({
           id: [this.userDetails.id, [Validators.required]],
           email: [this.userDetails.email, [Validators.required, Validators.email]],
           
            
          })
          this.useraddresses=JSON.parse(window.localStorage.getItem('userAddress'));
       // console.log(this.profileForm.value,"Profile");
       });  
        this.spinner.show();
            this.commonservice.postData({a:1},"seller/getCities").subscribe(data => {
             this.spinner.hide();    
            if(data.status)
            {
              this.cities=data.data.response;

            }

    },
    err => {
      console.log("Error occured.",err)
    });  
          
  
    this.commonservice.uploadPath.subscribe(file => {
    //console.log("File UPPPP",file);    
    if(file.id && file.upload_type=='avtar')
    {
        this.spinner.show();
       
        let body={id:this.userDetails.id,avatar_original:file.id};
        this.commonservice.postData(body,"seller/uploadprofilepic").subscribe(data => {
         this.spinner.hide();    
        if(data.status)
        {
             this.userDetails.avatar_dtls=file;
             this.userDetails.profile_img=file.file_name;
             window.localStorage.setItem('userDetails', JSON.stringify(this.userDetails));
             this.commonservice.SetUser();
        }
        },
        err => {
          console.log("Error occured.")
        });  
       
    }
        
    });
    
  }
 addAddress(content):void {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',size:'lg'}).result.then((res) => {
      
    }, (res) => {
      this.addressForm.reset(); 
      this.IsadSubmit=false;
    });   
 }
 onSubmit():void{
 this.IsSubmitted=true;
 if(this.profileForm.status=="VALID" )
 {
      this.spinner.show();
      this.commonservice.postData(this.profileForm.value,"seller/update-profile").subscribe(data => {
       this.spinner.hide();    
      if(data.status)
        {
           this.notifyService.showSuccess(data.message, "Profile")
           this.IsSubmitted=false;
           window.localStorage.setItem('userDetails', JSON.stringify(data.data));
           this.commonservice.SetUser();
           
        }
        else{
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Profile")
                }
        }
        },
        err => {
          console.log("Error occured.")
        });  
     
 }    
     
 }
 
 populatePostcode()
    {
    let body={city:this.addressForm.get('city').value,thana:this.addressForm.get('thana').value};
     this.spinner.show();
    this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
     this.spinner.hide();    
    if(data.status)
    {
        this.postalcodes=data.data.response;
            this.areas=[];
            this.addressForm.patchValue({
             postal_code:null,
             area:null,
          });  
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
  populateThana()
  {
        let body={city:this.addressForm.get('city').value};
        this.spinner.show();
        this.commonservice.postData(body,"seller/getThana").subscribe(data => {
         this.spinner.hide();    
        if(data.status)
        {
            this.thanas=data.data.response;
            this.postalcodes=[];
            this.areas=[];
            this.addressForm.patchValue({
             thana:null,
             postal_code:null,
             area:null,
          });  
        }
        },
        err => {
          console.log("Error occured.")
        });  
   }
   populateArea()
    {
    let body={postcode:this.addressForm.get('postal_code').value};
    this.spinner.show();
    this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
    this.spinner.hide();    
    if(data.status)
    {
        this.areas=data.data.response;
        this.addressForm.patchValue({
             area:null,
          });  
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
   
   submitAddress(): void{
   this.IsadSubmit=true;   
   if(this.addressForm.status=="VALID")
   {
       this.spinner.show();
       this.commonservice.postData(this.addressForm.value,"seller/addAddress").subscribe(data => {
           this.spinner.hide();
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "Profile")
               this.addressForm.reset(); 
               this.IsadSubmit=false;
               window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
               this.useraddresses=data.data.addresses;
               this.modalService.dismissAll();
            }
            else{
                    let errors=data.errors;
                    for (let error of errors) 
                    {
                     this.notifyService.showError(error, "Addrees")
                    }
            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
    
   }
setdefaultAddr(id) :void{
    let body={id:id,user_id:this.userDetails.id}
    this.commonservice.postData(body,"seller/setdefaultAddr").subscribe(data => {
    if(data.status)
    {
       window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
       this.useraddresses=data.data.addresses;
    }
    else{
            let errors=data.errors;
            for (let error of errors) 
            {
             this.notifyService.showError(error, "Addrees")
            }
    }
    },
    err => {
      console.log("Error occured.")
    });  
    
}
delAddress(id) :void{
this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove this address... ?')
    .then((confirmed) => {
    if(confirmed){    
    let body={id:id,user_id:this.userDetails.id}
    this.commonservice.postData(body,"seller/delAddress").subscribe(data => {
    if(data.status)
    {
       window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
       this.useraddresses=data.data.addresses;
    }
    else{
            let errors=data.errors;
            for (let error of errors) 
            {
             this.notifyService.showError(error, "Addrees")
            }
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }    
 uploadPhoto(accept: any,multiple:boolean,loadType: any,service_url: any):void{
    let body={acceptType:accept,multiple:multiple,loadType:loadType,service_url:service_url};    
    this.commonservice.uploadSetting.next(body);    
    this.fileuploadservice.gallery()
    .then((confirmed) => {

    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));   
    }
 
changeEmail(): void{
    this.IsemailSubmit=true;
    if(this.emailForm.status=="VALID")
   {
       this.commonservice.postData(this.emailForm.value,"seller/update_email").subscribe(data => {
        if(data.status)
        {
            this.IsemailSubmit=false;
            this.notifyService.showSuccess(data.message, "Profile")

        }
        else{
            this.notifyService.showError(data.message, "Profile")
        }
        },
        err => {
          console.log("Error occured.")
        });      
       }
    
}
delprofilePhoto()
{
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove profile photo... ?')
    .then((confirmed) => {
    if(confirmed){    
    let body={id:this.userDetails.id}
    this.commonservice.postData(body,"seller/delprofilePhoto").subscribe(data => {
    if(data.status)
    {
       this.userDetails.avatar_dtls=[];
       this.userDetails.profile_img=data.data.file_name;
       window.localStorage.setItem('userDetails', JSON.stringify(this.userDetails));
       this.commonservice.SetUser();
    }
    else{
        let errors=data.errors;
        for (let error of errors) 
        {
         this.notifyService.showError(error, "Addrees")
        }
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
}


}
