import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerPaymentHistoryComponent } from './seller-payment-history.component';

describe('SellerPaymentHistoryComponent', () => {
  let component: SellerPaymentHistoryComponent;
  let fixture: ComponentFixture<SellerPaymentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerPaymentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
