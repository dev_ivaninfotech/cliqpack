import { Component, OnInit,AfterViewInit,ViewChild  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';

import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
class searchFilter {
  name: "";
}
@Component({
  selector: 'app-seller-payment-history',
  templateUrl: './seller-payment-history.component.html',
  styleUrls: ['./seller-payment-history.component.css']
})
export class SellerPaymentHistoryComponent implements OnInit, AfterViewInit {
   userDetails:any;
   payments:any;
   start_index:any;
   dtOptions: DataTables.Settings = {};
   searchForm: searchFilter = new searchFilter();
   @ViewChild(DataTableDirective, {static: false})
   datatableElement: DataTableDirective;
   
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
  const that = this;
     this.commonservice.loggeduser.subscribe(user => {
     this.userDetails = user; 
     this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.id= this.userDetails.seller_id;  
       if(this.searchForm.name!=undefined)
       {
       dataTablesParameters.name=this.searchForm.name;
       }
        
       this.commonservice.postData(dataTablesParameters,"payment/sellerPyments").subscribe(resp => {
       if(resp.status)
        {
          this.payments=resp.data;
          this.start_index=resp.start_index
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      //columns: [{ data: 'sl' }, { data: 'name' }, { data: 'category' },{ data: 'qty' },{ data: 'stock' },{ data: 'unit_price' },{data: 'published'},{data: 'featured'},{data: 'action'}]
    };     
   });        
  }
  ngAfterViewInit(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
            
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
        $('select', this.footer()).on('change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }

}
