import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerProductReviewComponent } from './seller-product-review.component';

describe('SellerProductReviewComponent', () => {
  let component: SellerProductReviewComponent;
  let fixture: ComponentFixture<SellerProductReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerProductReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerProductReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
