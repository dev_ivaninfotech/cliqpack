import { Component, OnInit,AfterViewInit,ViewChild  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';

class searchFilter {
  code: any;
  payment_status: "";
  delivery_status: "";
}
@Component({
  selector: 'app-seller-order',
  templateUrl: './seller-order.component.html',
  styleUrls: ['./seller-order.component.css']
})
export class SellerOrderComponent implements OnInit, AfterViewInit {
   orderDetails:any;  
   userDetails:any;
   orders:any;
   start_index:any;
   dtOptions: DataTables.Settings = {};
   searchForm: searchFilter = new searchFilter();
   @ViewChild(DataTableDirective, {static: false})
   datatableElement: DataTableDirective;
   constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService) { 
   
   }

  ngOnInit(): void {
  this.searchForm.delivery_status="";   
  this.searchForm.payment_status=""; 
  const that = this;
     this.commonservice.loggeduser.subscribe(user => {
     this.userDetails = user; 
     this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.id= this.userDetails.id;  
       if(this.searchForm.code!=undefined)
       {
       dataTablesParameters.code=this.searchForm.code;
       }
        if(this.searchForm.payment_status!=undefined)
       {
       dataTablesParameters.payment_status=this.searchForm.payment_status;
       }
        if(this.searchForm.delivery_status!=undefined)
       {
       dataTablesParameters.delivery_status=this.searchForm.delivery_status;
       }
       this.commonservice.postData(dataTablesParameters,"seller/order").subscribe(resp => {
       if(resp.status)
        {
          this.orders=resp.data;
          this.start_index=resp.start_index
          let total_pending_order=parseInt(this.userDetails.total_pending_order);
          if(total_pending_order>0)
          {
              total_pending_order=total_pending_order-resp.total_pending_order;
              this.userDetails.total_pending_order=total_pending_order;
              window.localStorage.setItem('userDetails', JSON.stringify(this.userDetails));
              this.commonservice.SetUser();
          }
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
      columns: [{ data: 'sl' }, { data: 'code' }, { data: 'net_product' },{ data: 'name' },{ data: 'grand_total' },{ data: 'delivery_status' },{data: 'payment_status'},{data: 'action'}]
    };     
   });  
    
           
  }
 ngAfterViewInit(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
            
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
        $('select', this.footer()).on('change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }
  
  
}
