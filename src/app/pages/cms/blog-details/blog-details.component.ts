import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/notification.service';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent implements OnInit {
  blogURL: any = FormGroup;
  blogDetails: any = null;
  relatedBlogs: any = [];
  blogCategories: any = [];

  constructor(
    private http: HttpClient,
    private notifyService: NotificationService,
    private api: ServicesService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(routeParams => {
      this.blogURL = this.formBuilder.group({
        blogId: routeParams.blogId,
        blogSlug: routeParams.blogSlug,
      });
    });

    this.spinner.show();
    this.http.get<any>(this.api.api_url + 'blog/blog-details/' + this.blogURL.value.blogId)
    .subscribe((res: any) => {
      if (res.status == true) {
        this.blogDetails = res.Blog.blogDetails
        this.relatedBlogs = res.Blog.relatedBlogs
        this.blogCategories = res.Blog.blogCategory
      } else {
        this.notifyService.showWarning(res.message, 'Blogs API Exception');
      }
      this.spinner.hide();
    }, (error: any) => {
      this.notifyService.showWarning("Somthing went wrong !!!!", "Blogs API Error Occured");
      this.spinner.hide();
    });
  }
}
