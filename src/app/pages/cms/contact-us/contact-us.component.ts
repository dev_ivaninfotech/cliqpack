import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/notification.service';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactForm: FormGroup;
  captcha_code: any = Math.floor(1000 + Math.random() * 9000);

  constructor(
    public formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private http: HttpClient,
		private notifyService: NotificationService,
		private api: ServicesService,
  ) {
    this.contactForm = formBuilder.group({
      'first_name' : [null, Validators.compose([Validators.required])],
      'last_name' : [null, Validators.compose([Validators.required])],
      'email' : [null, Validators.compose([Validators.required, Validators.email])],
      'phone' : [null, Validators.compose([Validators.required])],
      'message' : [null, Validators.compose([Validators.required])],
      'subject' : [null, Validators.compose([Validators.required])],
      'captcha_code' : [null, Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {
  }

  contactSubmit(event: any){
    // if (this.contactForm.valid) {
      if(this.contactForm.value.captcha_code != this.captcha_code){
        this.notifyService.showError("The captcha you entered is invalid !!!!", "Error Message");
        return;
      }

      this.spinner.show();
      this.http.post<any>(this.api.api_url + 'contactSubmit', this.contactForm.value)
			.subscribe((res: any) => {
				if (res.status == true) {
					this.contactForm.reset();
					this.notifyService.showSuccess(res.message, 'Success Message');
				} else {
					this.notifyService.showError(res.message, 'Error Message');
				}
        this.spinner.hide();
			}, (error: any) => {
				this.notifyService.showError("Somthing went wrong !!!!", "Error Message");
        this.spinner.hide();
			});
    // }
  }
}
