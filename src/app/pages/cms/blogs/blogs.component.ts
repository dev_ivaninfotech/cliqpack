import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from 'src/app/notification.service';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
	blogsMaster: any = [];
	categoryWiseListing: boolean = false;
	blogCategory: any = null;

  constructor(
		private http: HttpClient,
		private notifyService: NotificationService,
		private api: ServicesService,
		private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    

    this.activatedRoute.params.subscribe(routeParams => {
      if(routeParams.categoryId){
        this.categoryWiseListing = true;
        this.fetchBlogsByCategories(routeParams.categoryId)
      } else{
        this.fetchBlogs()
      }
    });
  }

  fetchBlogs(){
    this.spinner.show();
    this.http.post<any>(this.api.api_url + 'blog/blog-list', {})
			.subscribe((res: any) => {
				if (res.status == true) {
					this.blogsMaster = res.allBlogs
				} else {
					this.notifyService.showWarning(res.message, 'Blogs API Exception');
				}
        this.spinner.hide();
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Blogs API Error Occured");
        this.spinner.hide();
			});
  }
  
  fetchBlogsByCategories(categoryId){
    this.spinner.show();
    this.http.post<any>(this.api.api_url + 'blog/blog-list', {'category_id': categoryId})
			.subscribe((res: any) => {
				if (res.status == true) {
					this.blogsMaster = res.allBlogs
					this.blogCategory = res.category
				} else {
					this.notifyService.showWarning(res.message, 'Blogs API Exception');
				}
        this.spinner.hide();
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Blogs API Error Occured");
        this.spinner.hide();
			});
  }

}
