import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  regForm: any = FormGroup;
  IsSubmitted:boolean=false;
  constructor(private route: Router, private notifyService: NotificationService, private http: HttpClient, private api: ServicesService, private formBuilder: FormBuilder,private commonservice:CommonService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  this.regForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });        
  }
onSubmit() {
    //console.log(this.regForm.value);
    this.IsSubmitted=true;
    if(this.regForm.status=="VALID" )
   {
        this.spinner.show();
        this.commonservice.postData(this.regForm.value,"forgot-password").subscribe(data => {
             this.spinner.hide();
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "Forgot Password")
               this.regForm.reset(); 
               this.IsSubmitted=false;
            }
            
            else{
                                         this.notifyService.showError(data.errors, "Forgot Password")

            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
    
}
}
