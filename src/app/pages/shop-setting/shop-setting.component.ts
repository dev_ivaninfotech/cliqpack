import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { FileUploadMultipleService } from '../../file-upload-multiple/file-upload-multiple.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import {ModalDismissReasons, NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-shop-setting',
  templateUrl: './shop-setting.component.html',
  styleUrls: ['./shop-setting.component.css']
})
export class ShopSettingComponent implements OnInit {
  shopForm: FormGroup;
  socialForm: FormGroup;
  IsSubmitted: boolean;
  IssocialSubmit: boolean;
  logo_preview_url:any;
  thanas:any;
  postalcodes:any;
  areas:any;
  cities:any;
  userDetails:any;
  shopDetails:any;
  banners:any;
  constructor(
  private commonservice:CommonService,private route: Router, private notifyService: NotificationService, private http: HttpClient, 
  private formBuilder: FormBuilder,private modalService: NgbModal,
  private confirmationDialogService: ConfirmationDialogService,
  private fileuploadservice: FileUploadMultipleService,private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
      this.spinner.show();
      const reg = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
      this.userDetails=JSON.parse(window.localStorage.getItem('userDetails'));  
      this.shopForm = this.formBuilder.group({
      id:['',Validators.required],
      name:['',Validators.required],
      city: ['',Validators.required],
      thana:['',Validators.required],
      postalcode:['',Validators.required],
      area:['',Validators.required],
      address:['',Validators.required],
      meta_title:['',Validators.required],
      meta_description:['',Validators.required],

    })
      
    this.socialForm = this.formBuilder.group({
      id:['',Validators.required],
      facebook:['',Validators.pattern(reg)],
      google: ['',Validators.pattern(reg)],
      twitter:['',Validators.pattern(reg)],
      youtube:['',Validators.pattern(reg)],

    })  
    let body={user_id:this.userDetails.id};
    this.commonservice.postData(body,"shopnew/view").subscribe(data => {
    if(data.status)
    {
        this.spinner.hide();
        this.shopDetails=data.data.response;
        this.commonservice.uploadPath.subscribe(file => {
        if(file.id && file.upload_type=='shoplogo')
        {
            let body={id:this.shopDetails.id,logo:file.id};
            this.commonservice.postData(body,"shopnew/updatesiteLogo").subscribe(data => {
            if(data.status)
            {
                 this.shopDetails.shop_logo_dtls=file;
            }
            },
            err => {
              console.log("Error occured.")
            });  

        }

        });  
        
         this.commonservice.uploadPath.subscribe(file => {
        if(file.id && file.upload_type=='banner')
        {
            let body={id:this.shopDetails.id,logo:file.id};
            this.commonservice.postData(body,"shopnew/updateSlider").subscribe(data => {
            if(data.status)
            {
                 this.banners.push(file);
            }
            },
            err => {
              console.log("Error occured.")
            });  

        }

        });  
       this.shopForm.patchValue(data.data.response) ;
       this.socialForm.patchValue(data.data.response) ;
       
       this.populateCity();
       this.populateThana(this.shopDetails.city)
       this.populatePostcode(this.shopDetails.city,this.shopDetails.thana)
       this.populateArea(this.shopDetails.postalcode)
       this.banners=data.data.response.banners;
    }
    },
    err => {
      console.log("Error occured.")
    });    
    
    
  }
  populateCity()
  {
      this.commonservice.postData({a:1},"seller/getCities").subscribe(data => {
      if(data.status)
        {
          this.cities=data.data.response;
        }

        },
        err => {
          console.log("Error occured.",err)
        });  
  }
   populateThana(city)
  {
        let body={city:city};
        this.commonservice.postData(body,"seller/getThana").subscribe(data => {
        if(data.status)
        {
            this.thanas=data.data.response;
            
            
        }
        },
        err => {
          console.log("Error occured.")
        });  
   }
   
   populateArea(postcode)
    {
    let body={postcode:postcode};
    this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
    if(data.status)
    {
        this.areas=data.data.response;
        
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
    populatePostcode(city,thana)
    {
    let body={city:city,thana:thana};
    this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
    if(data.status)
    {
        this.postalcodes=data.data.response;
        
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
    
   basicSubmit():void
   {
        this.IsSubmitted=true;
        if(this.shopForm.status=='VALID')
        {
        this.spinner.show();     
        this.commonservice.postData(this.shopForm.value,"shopnew/update-shop").subscribe(data => {
        this.spinner.hide();     
        if(data.status)
        {
           this.notifyService.showSuccess(data.message, "Shop Setting")
           this.IsSubmitted=false;
        }
        else{
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Shop Setting")
                }
        }
        },
        err => {
          console.log("Error occured.")
        });
      }  
   }
   
   socialSubmit():void
   {
        this.IssocialSubmit=true;
        if(this.socialForm.status=='VALID')
        {
        this.commonservice.postData(this.socialForm.value,"shopnew/updateSocial").subscribe(data => {
        if(data.status)
        {
           this.notifyService.showSuccess(data.message, "Shop Setting");
           this.IssocialSubmit=false;
        }
        else{
                let errors=data.errors;
                for (let error of errors) 
                {
                 this.notifyService.showError(error, "Shop Setting")
                }
        }
        },
        err => {
          console.log("Error occured.")
        });
      }  
   }
   
    
    uploadPhoto(accept: any,multiple:boolean,loadType: any,service_url: any):void{
    let body={acceptType:accept,multiple:multiple,loadType:loadType,service_url:service_url};    
    this.commonservice.uploadSetting.next(body);    
    this.fileuploadservice.gallery()
    .then((confirmed) => {

    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));   
    }
    
    
    changeCity() 
    {
        
        this.populateThana(this.shopForm.get('city').value)
    }
    changeThana() 
    {
        let city=this.shopForm.get('city').value;
        let thana=this.shopForm.get('thana').value;
        this.populatePostcode(city,thana)
    }
    changePostal() 
    {
        
        this.populateArea(this.shopForm.get('postalcode').value)
    }
     changeCityNew()
    {
        this.spinner.show();
        let city=this.shopForm.get('city').value;
        let body={city:city};
        this.commonservice.postData(body,"seller/getThana").subscribe(data => {
        if(data.status)
        {
            this.thanas=data.data.response;
            this.postalcodes=[];
            this.areas=[];
            this.shopForm.patchValue({
             thana:null,
             postalcode:null,
             area:null,
          });  
            
            
        }
        this.spinner.hide(); 
        },
        err => {
          console.log("Error occured.")
        });  
    }
    changeThanaNew()
    {
         this.spinner.show();
        let city=this.shopForm.get('city').value;
        let thana=this.shopForm.get('thana').value;
        let body={city:city,thana:thana};
        this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
        if(data.status)
        {
            this.postalcodes=data.data.response;
            this.areas=[];
            this.shopForm.patchValue({
             postalcode:null,   
             area:null,
          });  
           

        }
         this.spinner.hide(); 
        },
        err => {
          console.log("Error occured.")
        });  
    }
    
    changePostalNew()
    {
        this.spinner.show();
        let postcode=this.shopForm.get('postalcode').value;
        let body={postcode:postcode};
        this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
        if(data.status)
        {
            this.areas=data.data.response;
            this.shopForm.patchValue({
             area:null,
          });  
          

        }
        this.spinner.hide(); 
        },
        err => {
          console.log("Error occured.")
        });  
    }
    removeBanner(i):void
    {
        this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove this Banner... ?')
        .then((confirmed) => {
         if(confirmed){   
         let body={id:this.shopDetails.id,logo:this.banners[i].id};   
         this.commonservice.postData(body,"shopnew/deleteSlider").subscribe(data => {
        if(data.status)
        {
           

        }
        },
        err => {
          console.log("Error occured.")
        });  
         this.banners.splice(i, 1);
        }
        }
        )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
       
    }
    
}
