import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartnewComponent } from './cartnew.component';

describe('CartnewComponent', () => {
  let component: CartnewComponent;
  let fixture: ComponentFixture<CartnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
