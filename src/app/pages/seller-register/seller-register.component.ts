import { Component, OnInit,Input  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import { FileUploaderModule } from "ng4-file-upload";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.component.html',
  styleUrls: ['./seller-register.component.css']
})
export class SellerRegisterComponent implements OnInit {
  regForm: FormGroup;
  IsSubmitted: boolean;
  logo_preview_url:any;
  thanas:any;
  postalcodes:any;
  areas:any;
  cities:any
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService, private http: HttpClient, private formBuilder: FormBuilder,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.commonservice.postData({a:1},"seller/getCities").subscribe(data => {
            if(data.status)
            {
              this.cities=data.data.response;
            }
            
            },
            err => {
              console.log("Error occured.",err)
            });  
   this.regForm = this.formBuilder.group({
            name: ['', Validators.required],
            phone: ['',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirm_password: ['', [Validators.required,Validators.minLength(6)]],
            shop_name: ['', Validators.required],
            city: [null, Validators.required],
            thana: [null, Validators.required],
            postalcode: [null, Validators.required],
            area: [null, Validators.required],
            logo: ['', Validators.required],
            address: ['', Validators.required],
            checkbox_example_1: ['', Validators.required],
            file_original_name: ['', Validators.required],
            file_size: ['', Validators.required],
            extension: ['', Validators.required]
          },
          { 

      validator: ConfirmedValidator('password', 'confirm_password')

    }

          );     
  }
  onSubmit() : void{
   this.IsSubmitted=true; 
  
   if(this.regForm.status=="VALID" )
   {
        this.commonservice.postData(this.regForm.value,"seller/signup").subscribe(data => {
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "cliqPack")
               this.regForm.reset(); 
               this.IsSubmitted=false;
               this.logo_preview_url="";
            }
            else{
                    let errors=data.errors;
                    for (let error of errors) 
                    {
                     this.notifyService.showError(error, "cliqPack")
                    }
            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
  
  
  }
    get f(){

    return this.regForm.controls;

  }
  fileuploaderFileChange(files: FileList){
  let body={logo_file:files[0]};
  this.commonservice.postData(body,"seller/uploadLogo").subscribe(data => {
           
            this.regForm.patchValue({
            logo: data.data.logo,
            file_original_name: data.data.file_original_name,
            file_size: data.data.file_size,
            extension: data.data.extension
            
          });
            this.logo_preview_url=data.data.logo_preview_url
            },
            err => {
              console.log("Error occured.")
            });  
}
    populateThana()
    {
    let body={city:this.regForm.get('city').value};
    this.spinner.show();
    this.commonservice.postData(body,"seller/getThana").subscribe(data => {
    this.spinner.hide();    
    if(data.status)
    {
        this.thanas=data.data.response;
        this.postalcodes=[];
        this.areas=[];
        this.regForm.patchValue({
             postalcode:null,   
             area:null,
             thana:null
          });  
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
    populatePostcode()
    {
    let body={city:this.regForm.get('city').value,thana:this.regForm.get('thana').value};
     this.spinner.show();
    this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
     this.spinner.hide();    
    if(data.status)
    {
        this.postalcodes=data.data.response;
        this.areas=[];
        this.regForm.patchValue({
             area:null,
             postalcode:null
          }); 
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
    populateArea()
    {
    let body={postcode:this.regForm.get('postalcode').value};
     this.spinner.show();
    this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
      this.spinner.hide();   
    if(data.status)
    {
        this.areas=data.data.response;
    }
    },
    err => {
      console.log("Error occured.")
    });  
    } 

}
