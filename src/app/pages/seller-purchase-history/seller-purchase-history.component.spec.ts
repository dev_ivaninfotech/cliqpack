import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerPurchaseHistoryComponent } from './seller-purchase-history.component';

describe('SellerPurchaseHistoryComponent', () => {
  let component: SellerPurchaseHistoryComponent;
  let fixture: ComponentFixture<SellerPurchaseHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerPurchaseHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerPurchaseHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
