import { Component, OnInit,AfterViewInit,ViewChild  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";

class searchFilter {
  code: any;
  payment_status: "";
  delivery_status: "";
}
@Component({
  selector: 'app-seller-purchase-history',
  templateUrl: './seller-purchase-history.component.html',
  styleUrls: ['./seller-purchase-history.component.css']
})
export class SellerPurchaseHistoryComponent implements OnInit, AfterViewInit {

   orderDetails:any;  
   userDetails:any;
   orders:any;
   start_index:any;
   dtOptions: DataTables.Settings = {};
   searchForm: searchFilter = new searchFilter();
   @ViewChild(DataTableDirective, {static: false})
   datatableElement: DataTableDirective;
   constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private confirmationDialogService: ConfirmationDialogService,private spinner: NgxSpinnerService) { 
   
   }

 ngOnInit(): void {
  this.searchForm.delivery_status="";   
  this.searchForm.payment_status=""; 
  const that = this;
     this.commonservice.loggeduser.subscribe(user => {
     this.userDetails = user; 
     this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 15,
      serverSide: true,
      processing: true,
      searching:false,
      ajax: (dataTablesParameters: any, callback) => {
       dataTablesParameters.id= this.userDetails.id;  
       if(this.searchForm.code!=undefined)
       {
       dataTablesParameters.code=this.searchForm.code;
       }
        if(this.searchForm.payment_status!=undefined)
       {
       dataTablesParameters.payment_status=this.searchForm.payment_status;
       }
        if(this.searchForm.delivery_status!=undefined)
       {
       dataTablesParameters.delivery_status=this.searchForm.delivery_status;
       }
       this.commonservice.postData(dataTablesParameters,"seller/sellerpurchaseHistory").subscribe(resp => {
       if(resp.status)
        {
          this.orders=resp.data;
          this.start_index=resp.start_index
          
          callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });

        }

        },
        err => {
          console.log("Error occured.",err)
        });    
      },
    };     
   });  
    
           
  }
 ngAfterViewInit(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
            
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
        $('select', this.footer()).on('change', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
      });
    });
  }
  cancelOrder(id)
  {
      this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to cancel this Order... ?')
    .then((confirmed) => {
     this.spinner.show();   
     let body={id:atob(id)};
     this.commonservice.postData(body,"seller/deleteOrder").subscribe(data => {
    if(data.status)
    {
      this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
          });
      this.notifyService.showSuccess("This order has been cancelled successfully", "Purchase History") 
      this.spinner.hide();    
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
     })
   
     .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));  
  } 
}
