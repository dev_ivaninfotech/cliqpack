import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-category',
  directives: [Category],
  template: `
  <ul>
    <li *ngFor="node of treeData">
      {{node.text}}
      <app-category [treeData]="node.children"></app-category>
    </li>
</ul>
  `
})
export class CategoryComponent implements OnInit {
   @Input() treeData: [];
  constructor() { }

  ngOnInit(): void {
  }

}
