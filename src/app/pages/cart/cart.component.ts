import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl,FormBuilder,Validators,FormArray } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router,ActivatedRoute  } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from '../../service/common.service';
import { FilterPipe } from '../../pipe/filter.pipe'; // your pipe path 
import {ModalDismissReasons, NgbModal,NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  delivery_location:any='';
  default_location:any;
  delivery_location_id:any;
  userAddresses:any;
  orderForm: FormGroup;
  thanas:any;
  postalcodes:any;
  areas:any;
  cities:any;
  addressForm: FormGroup;
  userDetails:any;
  IsadSubmit:boolean;
  shippingcost:any;
  additional_cost:any;
  discount:any;
  net_payble:any;
  net_product_cost:any=0;
  net_discount:any=0;
  shipping_additional:any=0;
  payment_type:any='COD';
  payment_status:any='Pending';
  collections: any='';
  p: number;
  itemsPerPage:any = 10;
  totalItems: any;
  page_no:number=1;
  activeTab:any=1;
  productsName:any=[];
  constructor(private route: Router,private acroute: ActivatedRoute, private notifyService: NotificationService, private http: HttpClient,
  private spinner: NgxSpinnerService,private commonservice:CommonService,private formBuilder: FormBuilder,private modalService: NgbModal,private confirmationDialogService: ConfirmationDialogService) { }
  @ViewChild('ADDRESS') ADDRESS: ElementRef<HTMLElement>; 
  ngOnInit(): void {
  this.orderForm = this.formBuilder.group({
  address_id:['',Validators.required],
  payment_type:[this.payment_type,Validators.required],
  additionalCost:['',Validators.required],
  shipping:['',Validators.required],
  payment_status:[this.payment_status,Validators.required],
  grand_total:[this.payment_status,Validators.required],
  order_from:['web',Validators.required],
  coupon_discount:0,
  coupon_code:'',
  productsName:['',Validators.required],  
  

})    
this.commonservice.loggeduser.subscribe(user => {
        this.userDetails = user; 
        this.addressForm = this.formBuilder.group({
            id:[''],
            name:['',Validators.required],
            user_id:[this.userDetails.id,Validators.required],
            address:['',Validators.required],
            city: [null,Validators.required],
            thana:[null,Validators.required],
            postal_code:[null,Validators.required],
            area:[null,Validators.required],
            phone:['',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]],
            
          })
    this.userAddresses=JSON.parse(window.localStorage.getItem('userAddress'));
    let set_defaults=[1];
    let delivery_location = this.userAddresses.filter(function(itm){
    return set_defaults.indexOf(itm.set_default) > -1;
  });
   if(delivery_location.length>0)
   {
    this.default_location=delivery_location[0];
    this.delivery_location_id=this.default_location.id;
    this.delivery_location=this.default_location;
   }
    
    }); 
    this.commonservice.postData({a:1},"seller/getCities").subscribe(data => {
             this.spinner.hide();    
            if(data.status)
            {
              this.cities=data.data.response;

            }

    },
    err => {
      console.log("Error occured.",err)
    });
  if(this.delivery_location)
  {  
  this.shipingCharge(); 
  }
  this.getcartItems();      
  }

populatePostcode()
    {
    let body={city:this.addressForm.get('city').value,thana:this.addressForm.get('thana').value};
     this.spinner.show();
    this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
     this.spinner.hide();    
    if(data.status)
    {
        this.postalcodes=data.data.response;
            this.areas=[];
            this.addressForm.patchValue({
             postal_code:null,
             area:null,
          });  
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
    populatePostcodeInit(city,thana)
    {
    let body={city:city,thana:thana};
    this.commonservice.postData(body,"seller/getPostcode").subscribe(data => {
    if(data.status)
    {
        this.postalcodes=data.data.response;
           
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }
  populateThana()
  {
        let body={city:this.addressForm.get('city').value};
        this.spinner.show();
        this.commonservice.postData(body,"seller/getThana").subscribe(data => {
         this.spinner.hide();    
        if(data.status)
        {
            this.thanas=data.data.response;
            this.postalcodes=[];
            this.areas=[];
            this.addressForm.patchValue({
             thana:null,
             postal_code:null,
             area:null,
          });  
        }
        },
        err => {
          console.log("Error occured.")
        });  
   }
   
   populateThanaInit(city)
  {
        let body={city:city};
        this.commonservice.postData(body,"seller/getThana").subscribe(data => {
        if(data.status)
        {
            this.thanas=data.data.response;
           
        }
        },
        err => {
          console.log("Error occured.")
        });  
   }
   populateArea()
    {
    let body={postcode:this.addressForm.get('postal_code').value};
    this.spinner.show();
    this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
    this.spinner.hide();    
    if(data.status)
    {
        this.areas=data.data.response;
        this.addressForm.patchValue({
             area:null,
          });  
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }   
populateAreaInit(postcode)
    {
    let body={postcode:postcode};
    this.commonservice.postData(body,"seller/getArealist").subscribe(data => {
    this.spinner.hide();    
    if(data.status)
    {
        this.areas=data.data.response;
       
        
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }   
addAddress(content):void {
  this.addressForm.reset();   
  this.addressForm.patchValue({user_id:this.userDetails.id,id:''}); 
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',size:'lg'}).result.then((res) => {
      
    }, (res) => {
      this.addressForm.reset(); 
      this.IsadSubmit=false;
    }).catch(e => console.log("reject: "));   
 }

submitAddress(): void{
   this.IsadSubmit=true;   
   if(this.addressForm.status=="VALID")
   {
       this.spinner.show();
       this.commonservice.postData(this.addressForm.value,"seller/addAddress").subscribe(data => {
           this.spinner.hide();
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "Profile")
               this.addressForm.reset(); 
               this.IsadSubmit=false;
               window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
               this.userAddresses=data.data.addresses;
               this.modalService.dismissAll();
               
            }
            else{
                    let errors=data.errors;
                    for (let error of errors) 
                    {
                     this.notifyService.showError(error, "Addrees")
                    }
            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
    
   }
setdefaultAddr(id) :void{
    let body={id:id,user_id:this.userDetails.id}
     this.spinner.show(); 
    this.commonservice.postData(body,"seller/setdefaultAddr").subscribe(data => {
      this.spinner.hide();    
    if(data.status)
    {
       window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
       this.userAddresses=data.data.addresses;
       let set_defaults=[1];
        let delivery_location = this.userAddresses.filter(function(itm){
        return set_defaults.indexOf(itm.set_default) > -1;
      });
       if(delivery_location.length>0)
       {
        this.default_location=delivery_location[0];
        this.delivery_location_id=this.default_location.id;
        this.delivery_location=this.default_location;
       }
       this.shipingCharge();
    }
    else{
            let errors=data.errors;
            for (let error of errors) 
            {
             this.notifyService.showError(error, "Addrees")
            }
    }
    },
    err => {
      console.log("Error occured.")
    });  
    
}
delAddress(id) :void{
this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove this address... ?')
    .then((confirmed) => {
    if(confirmed){ 
    this.spinner.show();        
    let body={id:id,user_id:this.userDetails.id}
    this.commonservice.postData(body,"seller/delAddress").subscribe(data => {
    if(data.status)
    {
        this.spinner.hide(); 
       window.localStorage.setItem('userAddress', JSON.stringify(data.data.addresses));
       this.userAddresses=data.data.addresses;
    }
    else{
            let errors=data.errors;
            for (let error of errors) 
            {
             this.notifyService.showError(error, "Addrees")
            }
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  } 
  editAddress(id,content):void{
      
   this.spinner.show();        
    let body={id:id}
    this.commonservice.postData(body,"seller/viewAddress").subscribe(data => {
    if(data.status)
    {
        this.spinner.hide(); 
        let resp=data.data;
        this.populateThanaInit(resp.city);
        this.populatePostcodeInit(resp.city,resp.thana);
        this.populateAreaInit(resp.postal_code); 
        this.addressForm.patchValue(resp) ;
        this.shipingCharge();
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',size:'lg'}).result.then((res) => {
      
    }, (res) => {
      this.addressForm.reset(); 
      this.IsadSubmit=false; 
    }).catch(e => console.log("reject: "));   
    }
    else{
            let errors=data.errors;
            for (let error of errors) 
            {
             this.notifyService.showError(error, "Addrees")
            }
    }
    },
    err => {
      console.log("Error occured.")
    });     
      
  }
  shipingCharge()
  {
    this.commonservice.postData({a:1},"cart/getshippingchargeweb").subscribe(resp => {
    if(resp.status)
     {
       
       this.shippingcost=parseFloat(resp.data);
       this.additional_cost=parseFloat(resp.additionalCost);
       this.shipping_additional=this.shippingcost+this.additional_cost;
       this.orderForm.patchValue({
       address_id:this.delivery_location_id,additionalCost:this.additional_cost,shipping:this.shippingcost,grand_total:this.net_payble    
       }) ;
       if(this.collections.length>0)
       {
           this.totalCost();
       }
     
     }
     },
     err => {
       console.log("Error occured.",err)
     });
  } 
  
  getcartItems()
  {
     let body={page_no:1} 
     this.commonservice.postData(body,"carts/indexWeb").subscribe(resp => {
     if(resp.status)
     {
       this.collections=resp.data.products;
       this.totalItems=this.collections.length;
       this.totalCost();
       
     }
     },
     err => {
       console.log("Error occured.",err)
     });
     
  }
  getPage(page) {
  this.page_no=page; 
  this.getcartItems();   
  }
  changeQty(item,increment)
  {
     this.spinner.show(); 
     if(increment)
     {
       item.quantity=item.quantity+1;  
     } 
     else{
         item.quantity=item.quantity-1;
     }
     let body={cart_id:item.cart_id,quantity:item.quantity} 
     this.commonservice.postData(body,"carts/change-quantity-web").subscribe(resp => {
         
     if(resp.status)
     {
        this.spinner.hide();
       this.shipingCharge();
      
     }
     },
     err => {
       console.log("Error occured.",err)
     });
     
     
  }
  
  totalCost()
  {
       
       this.net_product_cost=0;
       this.net_discount=0;
       
       this.collections.forEach(item=>{
       let discount=parseFloat(item.mrp)-parseFloat(item.cartprice);  
     
       this.net_product_cost+=(parseFloat(item.mrp)*parseInt(item.quantity))
       if(discount>0)
       {
       this.net_discount+= discount*parseInt(item.quantity);
       }  
      })
      
       this.net_payble=(this.net_product_cost+this.shipping_additional)-this.net_discount;
      this.orderForm.patchValue({
       grand_total:this.net_payble    
       }) ;
       
       
  }
  deleteCart(id,i)
  {      
     this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to remove this item from cart... ?')
    .then((confirmed) => {
    if(confirmed){ 
    this.spinner.show();        
    let body={cart_id:id}
    this.commonservice.postData(body,"carts/remove").subscribe(data => {
         
    if(data.status)
    {
       this.spinner.hide();
       this.collections.splice(i, 1);
       this.shipingCharge();
       this.totalItems=this.collections.length;
       this.notifyService.showError("Item has been removed from cart lists", "Cart")
       this.commonservice.totalcartItem.next(this.collections.length);
    }
    else{
            
    }
    },
    err => {
      console.log("Error occured.")
    });  
    }    
    }
    )
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
      
  }
  submitOrder()
  {
      this.collections.forEach(item=>{
      this.productsName.push(item.name); 
      })
      this.orderForm.patchValue({
       productsName:this.productsName.join(","),    
       }) ;
       //console.log(this.orderForm.value);
       if(this.orderForm.status=="VALID"){
        console.log(this.orderForm.value);  
        this.spinner.show();        
            
            this.commonservice.postData(this.orderForm.value,"order/store").subscribe(data => {
            this.spinner.hide();    
            if(data.status)
            {
                if(this.payment_type=='COD')
                {
                this.notifyService.showSuccess("Your order has been placed successfully", "Order");
                 this.route.navigate(['/payment-success']);
                 window.localStorage.removeItem("device_token")
                 this.commonservice.totalcartItem.next(0);
                }
                else{
                    console.log(data.mainURL);
                    location.href=data.mainURL;
                }
            }
            else{
                    let errors=data.errors;
                    for (let error of errors) 
                    {
                     this.notifyService.showError(error, "Addrees");
                    }
            }
            },
            err => {
              console.log("Error occured.")
            }); 


          }
      
  }
  
  openAddress() {
    let el: HTMLElement = this.ADDRESS.nativeElement;
    el.click();
}
  Changepay(payment_type)
  {
       this.orderForm.patchValue({
       payment_type:payment_type    
       }) ;
  }
}
