import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/notification.service';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent implements OnInit {
	brandMaster: any = [];

  constructor(
		private http: HttpClient,
		private notifyService: NotificationService,
		private api: ServicesService,
  ) { }

  ngOnInit(): void {
    this.http.get<any>(this.api.api_url + 'brands')
			.subscribe((res: any) => {
				if (res.status == true) {
					this.brandMaster = res.data
				} else {
					this.notifyService.showWarning(res.message, 'Brands API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Brands API Error Occured");
			});
  }

  
}
