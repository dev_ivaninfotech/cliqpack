import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { Router,ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-customer-order-detail',
  templateUrl: './customer-order-detail.component.html',
  styleUrls: ['./customer-order-detail.component.css']
})
export class CustomerOrderDetailComponent implements OnInit {

  orderDetails:any; 
  userDetails:any; 
  approval:any;
  current_order_status:any;
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private activatedRoute: ActivatedRoute,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
      
  this.commonservice.loggeduser.subscribe(user => {
  this.userDetails = user; 
   let body={id:this.activatedRoute.snapshot.params.id};  
    this.spinner.show();
    this.commonservice.postData(body,"seller/orderDetail").subscribe(data => {
    this.spinner.hide();    
    if(data.status)
    {
      this.orderDetails=data.data;
      this.approval=this.orderDetails.approval;
      if(this.orderDetails.approval=='')
      {
          this.current_order_status="Pending";
      }
      else if(this.orderDetails.approval)
      {
          this.current_order_status="Approved";
      }
      else{
          this.current_order_status="Rejected";
      }
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });
  });    
                
  }
  changeOrderstatus()
  {
      console.log(this.approval);
      let body={id:atob(this.activatedRoute.snapshot.params.id),status:this.approval};  
        this.commonservice.postData(body,"seller/update_order_status").subscribe(data => {
        if(data.status)
        {
           if(this.approval)
           {
            this.current_order_status="Approved";   
            this.notifyService.showSuccess("This order has been accepted successfully", "Order Details")

           }
           else{
             this.current_order_status="Rejected";      
            this.notifyService.showError("This order has been rejected", "Order Details")
           }
        }

        },
        err => {
          console.log("Error occured.",err)
        });            
  }

}
