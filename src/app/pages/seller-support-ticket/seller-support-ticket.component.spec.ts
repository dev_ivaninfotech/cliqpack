import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerSupportTicketComponent } from './seller-support-ticket.component';

describe('SellerSupportTicketComponent', () => {
  let component: SellerSupportTicketComponent;
  let fixture: ComponentFixture<SellerSupportTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerSupportTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerSupportTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
