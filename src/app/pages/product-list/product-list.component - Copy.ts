import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products:any = [];
  start:any = 0;
  throttle:any = 300;
  scrollDistance:any = 1;
  scrollUpDistance:any = 2;
  direction:any = "";
  cat_name:any="";
  customOptions: OwlOptions;
  searchForm: any = FormGroup;
  brands:any="";
  cat_level:any;
  parents:any;
  
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private activatedRoute: ActivatedRoute,
  private confirmationDialogService: ConfirmationDialogService,private spinner: NgxSpinnerService,private formBuilder: FormBuilder) { }
  
  ngOnInit(): void {
  this.activatedRoute.params.subscribe(routeParams => {
	this.start=0;
        console.log(routeParams,'change_param');
        this.searchForm = this.formBuilder.group({
        search:routeParams.search,
        searchId:routeParams.searchId,
        start:this.start

        }); 
        this.listProductsInit(); 	
	});
  this.customOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots:false,
    autoplay:true,
    navSpeed: 900,
    navText: ['',''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  } 
 
    
           
   this.listProductsInit(); 
  }
  
  onScrollDown(ev) {
    // add another 20 items
    this.start=parseInt(this.start)+15;
    this.populateProducts('append');        
    this.direction = "down";
  }
  
    onUp(ev) {
    this.start=parseInt(this.start)+15;
    
    this.populateProducts('pre-append');
    this.direction = "up";
  }
  
  populateProducts(pushtype=''):void{
    this.searchForm.patchValue({
    start:this.start,
    });  
    this.commonservice.postData(this.searchForm.value,"products/filter-web").subscribe(resp => {
    if(resp.status)
     {
       
        let tmp_products=resp.data;
        if(tmp_products.length>0) {
        tmp_products.forEach(item=>{
        if(pushtype=='append')    
        {
        this.products.push(item);
        }
        else{
        this.products.unshift(item);
        }    
              
        })
        }

     }

     },
     err => {
       console.log("Error occured.",err)
     });   
      
  }
  listProductsInit():void{
    if(this.activatedRoute.snapshot.params.search=='category')
    {
        let cat_name=this.searchForm.get('searchId').value;
        let body={name:cat_name};
        this.commonservice.postData(body,"categories/detailCategory").subscribe(resp => {
        if(resp.status)
         {
            this.parents=resp.data.parents;
            this.cat_level=parseInt(this.parents.length)-1;
         }
         },
         err => {
           console.log("Error occured.",err)
         });       
    }  
    this.start=0;
    this.spinner.show();
    this.commonservice.postData(this.searchForm.value,"products/filter-web").subscribe(resp => {
    this.spinner.hide();    
    if(resp.status)
     {
        this.products=resp.data;
     }
     },
     err => {
       console.log("Error occured.",err)
     });       
  }
  

}
