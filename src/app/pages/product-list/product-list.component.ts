import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";
import { OwlOptions } from 'ngx-owl-carousel-o';
import { GetColorName } from 'hex-color-to-color-name';
import { Options, LabelType } from "@angular-slider/ngx-slider";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
  products: any = [];
  attributes: any = [];
  total_items: any = 0;
  initial_loaded: any = false;
  category: any = null;
  subCategories: any = [];
  pagebanner: any = null;
  minPriceFilter: any = [];
  maxPriceFilter: any = [];
  parents: any;
  breadcrumbValue: any = null;

  start: any = 0;
  throttle: any = 300;
  scrollDistance: any = 1;
  scrollUpDistance: any = 2;
  direction: any = "";
  cat_name: any = "";
  customOptions: OwlOptions;
  searchForm: any = FormGroup;
  productSuffleForm: any = FormGroup;
  brands: any = "";
  cat_level: any;
  userDetails: any;

  priceRangeMinValue: number = 0;
  priceRangeMaxValue: number = 0;
  priceRangeOptions: Options = {
    floor: 0,
    ceil: 0,

    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return "<b>Min price:</b> &#2547;" + value;
        case LabelType.High:
          return "<b>Max price:</b> &#2547;" + value;
        default:
          return "&#2547;" + value;
      }
    }
  };

  constructor(private commonservice: CommonService, private route: Router, private notifyService: NotificationService, private activatedRoute: ActivatedRoute, private confirmationDialogService: ConfirmationDialogService, private spinner: NgxSpinnerService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.commonservice.loggeduser.subscribe(user => {
      this.userDetails = user;
    }, err => {
      console.log("Error occured.", err)
    });

    this.customOptions = {
      loop: true,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      autoplay: false,
      navSpeed: 900,
      navText: ['', ''],
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 2
        },
        740: {
          items: 3
        },
        940: {
          items: 4
        }
      },
      nav: true
    }

    let searchkeyword = "";
    this.activatedRoute.queryParams.subscribe(
      params => searchkeyword = params['keyword']
    );

    this.activatedRoute.params.subscribe(routeParams => {
      this.searchForm = this.formBuilder.group({
        search: routeParams.search,
        searchId: routeParams.searchId,
      });
    });

    this.productSuffleForm = this.formBuilder.group({
      "page_no": [1],
      "product_name": [(searchkeyword) ? searchkeyword : ''],
      "category_id": [''],
      "brand_id": [''],
      "brandId": [''],
      "color": [''],
      "attributesValue": [''],
      "sort_by": [''],
      "price_high_to_low": [''],
      "price_low_to_high": [''],
      "min_price": [''],
      "max_price": [''],
      "listingKey": [''],
    });

    this.listProductsInit();

    this.activatedRoute.params.subscribe(routeParams => {
      this.products = [];
      this.attributes = [];
      this.total_items = 0;
      this.initial_loaded = false;
      this.category = null;
      this.subCategories = [];
      this.pagebanner = null;
      this.minPriceFilter = [];
      this.maxPriceFilter = [];
      this.parents = null;
      this.breadcrumbValue = null;

      this.priceRangeMinValue = 0;
      this.priceRangeMaxValue = 0;

      this.priceRangeOptions = {
        floor: 0,
        ceil: 0,

        translate: (value: number, label: LabelType): string => {
          switch (label) {
            case LabelType.Low:
              return "<b>Min price:</b> &#2547;" + value;
            case LabelType.High:
              return "<b>Max price:</b> &#2547;" + value;
            default:
              return "&#2547;" + value;
          }
        }
      }

      this.productSuffleForm.patchValue({
        'page_no': 1,
        "product_name": [(searchkeyword) ? searchkeyword : ''],
        'category_id': '',
        'brand_id': '',
        'brandId': '',
        'color': '',
        'attributesValue': '',
        'sort_by': '',
        'price_high_to_low': '',
        'price_low_to_high': '',
        'min_price': '',
        'max_price': '',
        'listingKey': '',
      });

      this.searchForm = this.formBuilder.group({
        search: routeParams.search,
        searchId: routeParams.searchId,
      });

      this.listProductsInit();
    })
  }

  onScrollDown(ev) {
    if (this.initial_loaded == true) {
      this.productSuffleForm.patchValue({
        'page_no': parseInt(this.productSuffleForm.value.page_no) + 1,
      });

      this.browseProducts(false, 'pushitems');
    }
  }

  listProductsInit(): void {
    if (this.activatedRoute.snapshot.params.search == 'category') {
      let cat_slug = this.searchForm.get('searchId').value;
      let body = { slug: cat_slug };

      this.commonservice.postData(body, "categories/detailCategory").subscribe(resp => {
        if (resp.status) {
          this.pagebanner = resp.data.banner;
          this.parents = resp.data.parents;
          this.cat_level = parseInt(this.parents.length) - 1;
          this.productSuffleForm.patchValue({
            'category_id': resp.data.id
          });

          this.browseProducts();
        }
      },
        err => {
          console.log("Error occured.", err)
        });
    } else if (this.activatedRoute.snapshot.params.search == 'brand') {
      let brand_slug = this.searchForm.get('searchId').value;
      let body = { slug: brand_slug };

      this.commonservice.postData(body, "brand/details").subscribe(resp => {
        if (resp.status) {
          this.productSuffleForm.patchValue({
            'brand_id': resp.data.brand.id,
          });

          this.parents = "customText";
          this.breadcrumbValue = resp.data.brand.name;

          this.browseProducts();
        }
      },
        err => {
          console.log("Error occured.", err)
        });
    } else if (this.activatedRoute.snapshot.params.search == 'group') {
      let slug = "";
      this.parents = "customText";

      switch (this.searchForm.get('searchId').value) {
        case 'featured':
          slug = 'featured';
          this.breadcrumbValue = 'Featured Products';
          break;

        case 'flash-sale':
          slug = 'flashSale';
          this.breadcrumbValue = 'Flash Sale';
          break;

        case 'today-deals':
        case 'biggest-deals':
          slug = 'biggestDeals';
          this.breadcrumbValue = 'Biggest Deals';
          break;

        default:
          break;
      }

      this.productSuffleForm.patchValue({
        'listingKey': slug,
      });

      this.browseProducts();
    } else if (this.activatedRoute.snapshot.params.search == 'search') {
      this.parents = 'searchsection'
      this.browseProducts();
    }
  }

  browseProducts(resetattribute: any = true, fetchtype: any = 'initialload') {
    if (fetchtype == 'initialload') {
      this.spinner.show();
    }

    this.commonservice.postData(this.productSuffleForm.value, "products/browse")
      .subscribe(resp => {
        if (resp.status == true) {
          if (fetchtype == 'initialload') {
            this.products = resp.data.products;
            this.initial_loaded = true;
            this.total_items = resp.data.total_items;

            this.category = resp.data.category;
            this.subCategories = resp.data.subCategories;
          } else {
            console.log(resp.data.products);
            resp.data.products.forEach(element => {
              this.products.push(element);
            });
          }

          if (resetattribute === true) {
            this.priceRangeMinValue = resp.data.price_range.min;
            this.priceRangeMaxValue = resp.data.price_range.max;

            this.priceRangeOptions = {
              floor: resp.data.price_range.min,
              ceil: resp.data.price_range.max,

              translate: (value: number, label: LabelType): string => {
                switch (label) {
                  case LabelType.Low:
                    return "<b>Min price:</b> &#2547;" + value;
                  case LabelType.High:
                    return "<b>Max price:</b> &#2547;" + value;
                  default:
                    return "&#2547;" + value;
                }
              }
            }

            this.attributes = resp.attribute;
          }
        } else {
          console.log("Error occured.", resp.message)
        }

        if (fetchtype == 'initialload') {
          this.spinner.hide();
        }
      },
        err => {
          console.log("Error occured.", err)
          if (fetchtype == 'initialload') {
            this.spinner.hide();
          }
        });
  }

  filterChanged() {
    let colorselected = "";
    $('input[name="colorFilter[]"]:checked').each(function () {
      colorselected += $(this).val() + ','
    });

    let brandselected = "";
    $('input[name="brandFilter[]"]:checked').each(function () {
      brandselected += $(this).val() + ','
    });

    let attributeFilter = "";
    $('input[name="attributeFilter[]"]:checked').each(function () {
      attributeFilter += $(this).val() + ','
    });

    let min_price = this.priceRangeMinValue;
    let max_price = this.priceRangeMaxValue;

    this.productSuffleForm.patchValue({
      'page_no': 1,
      'color': colorselected,
      'brandId': brandselected,
      'attributesValue': attributeFilter,
      'min_price': min_price,
      'max_price': max_price,
    });

    this.browseProducts(false);
  }

  sorting() {
    let sort_by = "";
    let price_high_to_low = "";
    let price_low_to_high = "";

    switch ($('[name="sort_by"]').val()) {
      case 'newest':
        sort_by = "2";
        break;

      case 'price-asc':
        price_low_to_high = "1";
        break;

      case 'price-desc':
        price_high_to_low = "1";
        break;
    }

    this.productSuffleForm.patchValue({
      'page_no': 1,
      'sort_by': sort_by,
      'price_high_to_low': price_high_to_low,
      'price_low_to_high': price_low_to_high,
    });

    this.browseProducts(false);
  }

  shuffleMinMaxPriceFilterValues(minPrice, maxPrice) {
    this.minPriceFilter = [];
    this.maxPriceFilter = [];

    this.minPriceFilter.push({ 'value': minPrice });

    let price = (Math.ceil(minPrice / 1000) * 1000)
    for (let index = price; index <= maxPrice;) {
      this.minPriceFilter.push({ 'value': index });
      this.maxPriceFilter.push({ 'value': index });

      index = index + 1000;
    }
  }

  parseNumber(num) {
    return parseInt(num)
  }

  addToWishList(item) {
    if (this.userDetails == null) {
      this.notifyService.showError("Please login first", "Wishlist")
      this.route.navigate(['/login']);
    }
    else {
      this.spinner.show();
      let body = { product_id: item.id };
      this.commonservice.postData(body, "wishlist/add").subscribe(resp => {
        if (resp.status) {
          item['iswishlist'] = 1;
          this.notifyService.showSuccess(resp.message, "Success Message");
        } else {
          this.notifyService.showError(resp.message, "Error Message");
        }
        this.spinner.hide();
      },
        err => {
          console.log("Error occured.", err)
          this.spinner.hide();
        });
    }
  }

  removeFromWishlist(item) {
    if (this.userDetails == null) {
      this.notifyService.showError("Please login first", "Wishlist")
      this.route.navigate(['/login']);
    }
    else {
      this.spinner.show();
      let body = { product_id: item.id };
      this.commonservice.postData(body, "wishlist/remove").subscribe(resp => {
        if (resp.status) {
          item['iswishlist'] = 0;
          this.notifyService.showSuccess(resp.message, "Success Message");
        } else {
          this.notifyService.showError(resp.message, "Error Message");
        }
        this.spinner.hide();
      },
        err => {
          console.log("Error occured.", err)
          this.spinner.hide();
        });
    }
  }

  getColorName(colorcode) {
    return GetColorName(colorcode.replace('#', ''));
  }
}
