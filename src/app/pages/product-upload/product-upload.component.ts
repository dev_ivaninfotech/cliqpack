import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { FileUploadMultipleService } from '../../file-upload-multiple/file-upload-multiple.service';
import { FormGroup, FormControl, FormBuilder, Validators,FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { TreeviewItem } from 'ngx-treeview';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-product-upload',
  templateUrl: './product-upload.component.html',
  styleUrls: ['./product-upload.component.css']
})
export class ProductUploadComponent implements OnInit {
  categories:any
  barands:any;
  colors:any;
  attributes:any ;
  weights:any;
  productsizeweights:any=[];
  refunds:any=[];
  colors_active:any;
  productForm: FormGroup;
  choices: any = [];
  tax_types:any=[];
  userDetails:any;
  video_providers:any;
  is_barcode:boolean;
  is_refund:boolean;
  discount_types:any=[];
  attribute_values:any=[];
  photos:any=[];
  thumbnail_imgs:any=[];
  meta_images:any=[];
  pdf_file:any;
  IsSubmitted:boolean=false;
  variations:any=[];
  brands:any="";
  specifications:any=[];
  sub_specifications:any=[];
  subcategories:any;
  shiping_charge_types:any;  
  products:any;  
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '200px',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      
     
    
};

  constructor(
  private commonservice:CommonService,private route: Router, private notifyService: NotificationService, private http: HttpClient,private formBuilder: FormBuilder,
             private fileuploadservice: FileUploadMultipleService,private router: Router,private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
  this.commonservice.loggeduser.subscribe(user => {
  this.userDetails = user;     
  this.colors_active=0;    
  this.productsizeweights=[{text:'Standard',value:'standard'},{text:'Special',value:'special'},{text:'Large',value:'large'}] ;
  this.refunds=[{value:1,text:'Yes'},{value:0,text:'No'}]; 
  this.tax_types=[{text:'Flat',value:'amount'},{value:'percent',text:'Percent'}]; 
  this.discount_types=[{text:'Flat',value:'amount'},{value:'percent',text:'Percent'}]; 
  this.shiping_charge_types=[{text:'Weight Based',value:'weightbased'},{text:'Item Based',value:'itembased'}]; 

  this.video_providers=[
  {text:'Youtube',value:'youtube'},{text:'Dailymotion',value:'dailymotion'},{text:'Vimeo',value:'vimeo'}    
  ] 
  this.commonservice.postData('',"categories/treeView").subscribe(data => {
    if(data.status)
    {
      this.categories=data.data;
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });  
   this.commonservice.postData('',"color/listColors").subscribe(data => {
    if(data.status)
    {
      this.colors=data.data;
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });     
    this.commonservice.postData('',"attribute/index").subscribe(data => {
    if(data.status)
    {
      this.attributes=data.data;
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
    
    this.commonservice.postData('',"attribute/listWeightrange").subscribe(data => {
    if(data.status)
    {
      this.weights=data.data;
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });      
    this.productForm = this.formBuilder.group({
            name:['',Validators.required],
            added_by:['seller'],
            user_id: [this.userDetails.id],
            category_id:[null,Validators.required],
            sub_category_id:[null,Validators.required],
            brand_id:[''],
            current_stock:['',Validators.required],
            barcode:[''],
            productsizeweight:['standard',Validators.required],
            weightrange:['',Validators.required],
            refund:['',Validators.required],
            photos:this.formBuilder.array([]),
            thumbnail_imgs:this.formBuilder.array([]),
            unit:['',Validators.required],
            unit_price:['',Validators.required],
            min_qty:['',[Validators.required,Validators.min(1)]],
            tags:[''],
            description:[''],
            video_provider:[''],
            video_link:[''],
            purchase_price:['',Validators.required],
            tax:[0,Validators.required],
            tax_type:['amount'],
            discount:['',Validators.required],
            discount_type:['amount'],
            shipping_type:[''],
            deliverd_by:[''],
            isreturnreplace:[''],
            refund_replace_day:[''],
            flat_shipping_cost:[''],
            meta_title:[''],
            meta_description:[''],
            meta_images:this.formBuilder.array([]),
            pdf:[''],
            colors_active:[''],
            colors:[''],
            choice_attributes:[],
            attribute_values:this.formBuilder.array([]), 
            variations:this.formBuilder.array([]),
            specifications:this.formBuilder.array([]),
            sub_specifications:this.formBuilder.array([]),
            before:[''],
            weight:['',Validators.required],
            additional_type:['',Validators.required],
            existing_p_id:[null],
          })  
          
            this.commonservice.postData('',"product/index").subscribe(data => {
            if(data.status)
            {
              this.is_barcode=data.is_barcode;
              this.is_refund=data.is_refund;
            }

            },
            err => {
              console.log("Error occured.",err)
            });  
            this.commonservice.uploadPath.subscribe(file => {
            if(file.id && file.upload_type=='gallery')
            {
               this.pushPhoto(file); 
            }
            if(file.id && file.upload_type=='thumb')
            {
               this.pushThumb(file); 
            }
            if(file.id && file.upload_type=='meta_img')
            {
               this.pushMeta(file); 
            }
            if(file.id && file.upload_type=='pdf')
            {
               this.pushPdf(file); 
            }
        });  
            
  })    
  
  }
 newattributeValue(): FormGroup {
  return this.formBuilder.group({
    id: ['', Validators.required],
    attr_name: [''],
    attr_value: [''],
  });
}
newVariant(): FormGroup {
  return this.formBuilder.group({
    id: [''],
    variant:['',Validators.required],
    price: ['',Validators.required],
    sku: [''],
    qty: [10,Validators.required],
   
  });
}


newGallery(): FormGroup {
  return this.formBuilder.group({
    id: [''],
    file_original_name: [''],
    extension: [''],
    file_name: [''],
    file_size: [''],

  });
}
newThumb(): FormGroup {
  return this.formBuilder.group({
    id: [''],
    file_original_name: [''],
    extension: [''],
    file_name: [''],
    file_size: [''],

  });
}
newmetaImg(): FormGroup {
  return this.formBuilder.group({
    id: [''],
    file_original_name: [''],
    extension: [''],
    file_name: [''],
    file_size: [''],

  });
}
newSpecification(): FormGroup {
  return this.formBuilder.group({
    specification: [''],
    categoryspecification_id: [''],
    description: [''],
  });
}
newsubSpecification(): FormGroup {
  return this.formBuilder.group({
    specification: [''],
    categoryspecification_id: [''],
    description: [''],
  });
}
addAttr(item): void {
this.attribute_values = this.productForm.get('attribute_values') as FormArray  
let attributeForm=this.newattributeValue();
attributeForm.patchValue({
    id: item.id,
    attr_name: item.name,
  });  
this.attribute_values.push(attributeForm);
   
}
removeAttr(item):void{
this.attribute_values.removeAt(this.attribute_values.value.findIndex(attr => attr.id === item.id))
if(this.variations.length>0){
this.variations.clear();  
}
}
removeallAttr(): void{
if(this.attribute_values.length>0)
{    
this.attribute_values.clear(); 
}
if(this.variations.length>0){
this.variations.clear(); 
}
this.productForm.patchValue({
  choice_attributes:""
  
  });    
}
onSubmit(): void {
 this.IsSubmitted=true;   
 console.log(this.productForm.value);
 if(this.productForm.status=='VALID')
 {
  this.spinner.show();   
 this.commonservice.postDataRaw(this.productForm.value,"product/uploadProduct").subscribe(data => { 
     this.spinner.hide();   
if(data.status)
{
   this.notifyService.showSuccess(data.message, "Product")
   this.productForm.reset(); 
   this.IsSubmitted=false;
   this.commonservice.uploadSetting.next('');    
   this.commonservice.uploadPath.next('');  
   this.router.navigate(['/seller/product-listing']);  

}
else{
        let errors=data.errors;
        for (let error of errors) 
        {
         this.notifyService.showError(error, "Product")
        }
}
},
err => {
  console.log("Error occured.")
});  
 
 
 }  
}
skuCombination():void{
    
let body={colors:this.productForm.get('colors').value,colors_active:this.productForm.get('colors_active').value,attribute_values:JSON.stringify(this.productForm.get('attribute_values').value),unit_price:this.productForm.get('unit_price').value}; 
if(this.productForm.get('attribute_values').value!=''){
this.commonservice.postData(body,"product/skuCombination").subscribe(data => {
if(data.status)
{
  if(this.variations.length>0)  
  {
  this.variations.clear();   
  }
  let variants=data.data;
  variants.forEach(item=>this.pushVariant(item))
}

},
err => {
  console.log("Error occured.",err)
}); 
} 
}

uploadPhoto(accept: any,multiple:boolean,loadType: any,service_url: any):void{
let body={acceptType:accept,multiple:multiple,loadType:loadType,service_url:service_url};    
this.commonservice.uploadSetting.next(body);    
this.fileuploadservice.gallery()
.then((confirmed) => {

}
)
.catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));   
}
pushPhoto(item):void{
this.photos = this.productForm.get('photos') as FormArray  
let galleryForm=this.newGallery();   
galleryForm.patchValue({
    id:item.id,
    file_original_name: item.file_original_name,
    extension: item.extension,
    file_name: item.file_name,
    file_size: item.file_size,   
  });  
this.photos.push(galleryForm); 
}
pushThumb(item):void{
this.thumbnail_imgs = this.productForm.get('thumbnail_imgs') as FormArray  
let thumbnailForm=this.newThumb();   
thumbnailForm.patchValue({
    id:item.id,
    file_original_name: item.file_original_name,
    extension: item.extension,
    file_name: item.file_name,
    file_size: item.file_size,   
  });  
this.thumbnail_imgs.push(thumbnailForm); 
    
}
pushMeta(item):void{
this.meta_images = this.productForm.get('meta_images') as FormArray  
let metaForm=this.newmetaImg();   
metaForm.patchValue({
    id:item.id,
    file_original_name: item.file_original_name,
    extension: item.extension,
    file_name: item.file_name,
    file_size: item.file_size,   
  });  
this.meta_images.push(metaForm); 
    
}


removeGallery(index:any):void{
this.photos.removeAt(index)
    
} 

removeThumb(index:any):void{
this.thumbnail_imgs.removeAt(index);
} 
removeMeta(index:any):void{
this.meta_images.removeAt(index);   
} 
pushPdf(item):void{
this.pdf_file=item;   
this.productForm.patchValue({
    pdf:item.id,
   
  });  
} 
removePdf():void{
this.pdf_file=""; 
this.productForm.patchValue({
    pdf:"",
   
  });    
} 
categoryDetail(cat):void
{
    console.log(cat);
}
pushVariant(item):void{
this.variations = this.productForm.get('variations') as FormArray  
let variantForm=this.newVariant();   
variantForm.patchValue({
  variant:item
  
  });  
this.variations.push(variantForm); 
}
deleteVariant(i:any):void
{
    this.variations.removeAt(i);   
}


clearAll():void{
  if(this.productForm.get('colors_active').value) { 
  this.removeallAttr();
  
  this.productForm.patchValue({
  colors:""
  
  });  
  }
}
pushSpecification(item):void{
this.specifications = this.productForm.get('specifications') as FormArray  
let specificationForm=this.newSpecification();   
        specificationForm.patchValue({
        specification:item.specification,
        categoryspecification_id:item.id,
        description:''  
  });  
this.specifications.push(specificationForm); 
}
pushsubSpecification(item):void{
this.sub_specifications = this.productForm.get('sub_specifications') as FormArray  
let subspecificationForm=this.newsubSpecification();   
subspecificationForm.patchValue({
        specification:item.specification,
        categoryspecification_id:item.id,
        description:''  
  
  });  
this.sub_specifications.push(subspecificationForm); 
}


populateBrand(cat_id)
{
    if(this.sub_specifications.length>0)
    {
        this.sub_specifications.clear();
    }
    if(this.specifications.length>0)
    {
        this.specifications.clear();
    }
    
    
    this.commonservice.postData({id:cat_id.value},"brands/populateBrand").subscribe(data => {
    if(data.status)
    {
      
      this.brands=data.data;
      
      
    }

    },
    err => {
      console.log("Error occured.",err)
    }); 
    
    this.commonservice.postData({id:cat_id.value},"categories/getCategory").subscribe(data => {
    if(data.status)
    {
      if(data.data.length>0) 
      {
      let tmpspecifications=data.data;
      tmpspecifications.forEach(item=>this.pushSpecification(item));

      }
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
    
       
}
populateChild(item)
{
    
    if(item.children.length>0)
    {
        this.subcategories=item.children;
        this.productForm.patchValue({
        sub_category_id:null
        });  
        
    }
    else{
        this.subcategories="";
        this.productForm.patchValue({
        sub_category_id:item.value

        });  
        
    }
}


populatesubBrand(cat_id)
{
    this.commonservice.postData({id:cat_id.value},"brands/populateBrand").subscribe(data => {
    if(data.status)
    {
      if(data.data.length>0) 
      {
      this.brands=data.data;
      }
      
    }

    },
    err => {
      console.log("Error occured.",err)
    }); 
    
    this.commonservice.postData({id:cat_id.value},"categories/getCategory").subscribe(data => {
    if(data.status)
    {
      if(data.data.length>0) 
      {
      let tmpspecifications=data.data;
      tmpspecifications.forEach(item=>this.pushsubSpecification(item));

      }
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
    
       
}


populateProduct(item):void{
this.commonservice.postData({category_id:item.value},"product/listallProducts").subscribe(data => {
    if(data.status)
    {
      this.products=data.data;
    }

    },
    err => {
      console.log("Error occured.",err)
    });        
}

copyProduct(item):void
{
    this.spinner.show();  
    this.commonservice.postData({id:btoa(item.id)},"product/details").subscribe(data => {
     
    if(data.status)
    {
        this.productForm.patchValue(data.data) ;
        this.productForm.patchValue({
        user_id:this.userDetails.id,
        });    
        
         this.photos = this.productForm.get('photos') as FormArray
        if(this.photos.length>0){
        this.photos.clear();
        
        }
        
        let photos=data.data.photos;
        photos.forEach(item=>this.pushPhoto(item));
       
        this.thumbnail_imgs = this.productForm.get('thumbnail_imgs') as FormArray 
        if(this.thumbnail_imgs.length>0){
        this.thumbnail_imgs.clear();
        } 
       
        let thumbnail_imgs=data.data.thumbnail_imgs;
        thumbnail_imgs.forEach(item=>this.pushThumb(item));
        
        this.meta_images = this.productForm.get('meta_images') as FormArray
        if(this.meta_images.length>0){
        this.meta_images.clear();
        } 
        let meta_images=data.data.meta_images;
        meta_images.forEach(item=>this.pushMeta(item));
        if(data.data.pdf_file!=''){
        this.pdf_file=data.data.pdf_file;
        }
        
        let attribute_values=data.data.attribute_values;
        this.attribute_values = this.productForm.get('attribute_values') as FormArray  
        if(this.attribute_values.length>0){
        this.attribute_values.clear();
        }
        attribute_values.forEach(item=>{
        let attributeForm=this.newattributeValue();
         attributeForm.patchValue(item);  
         this.attribute_values.push(attributeForm);    
        });
        this.variations = this.productForm.get('variations') as FormArray;
        if(this.variations.length>0){
        this.variations.clear();
        }
        let variations=data.data.variations;
        variations.forEach(item=>{
          
        let variantForm=this.newVariant();   
        variantForm.patchValue(item);  
        this.variations.push(variantForm); 
        });
        this.specifications = this.productForm.get('specifications') as FormArray  
        if(this.specifications.length>0){
         this.specifications.clear();
        }
        let specifications=data.data.specifications_new;
        this.specifications.removeAt(0);
        specifications.forEach(item=>{
        let specificationForm=this.newSpecification();   
        specificationForm.patchValue(item);  
        this.specifications.push(specificationForm); 
           
        });
        this.sub_specifications = this.productForm.get('sub_specifications') as FormArray  
        if(this.sub_specifications.length>0){
        this.sub_specifications.clear(); 
        }   
        let sub_specifications=data.data.sub_specifications_new;
        this.sub_specifications.removeAt(0);
        sub_specifications.forEach(item=>{
        let subspecificationForm=this.newsubSpecification();   
        subspecificationForm.patchValue(item);  
        this.sub_specifications.push(subspecificationForm); 
             
        });
        
        //this.productForm.get('photos').setValue(data.data.photos);
        this.spinner.hide();
    }

    },
    err => {
      console.log("Error occured.",err)
    });        
}
resetForm():void {
    let category_id=this.productForm.get('category_id').value
     this.productForm.reset(); 
     this.pdf_file="";
     this.productForm.patchValue({
        user_id:this.userDetails.id,
        category_id:category_id,
        added_by:'seller',
        tax_type:'amount',discount_type:'amount',brand_id:'',additional_type:'',video_provider:'',tax:0,productsizeweight:'standard',refund:''
        });    
        if(this.photos.length>0){
        this.photos.clear();
        
        }
        if(this.thumbnail_imgs.length>0){
        this.thumbnail_imgs.clear();
        } 
        if(this.meta_images.length>0){
        this.meta_images.clear();
        } 
         if(this.attribute_values.length>0){
        this.attribute_values.clear();
        }
        if(this.variations.length>0){
        this.variations.clear();
        }
        if(this.specifications.length>0){
         this.specifications.clear();
        }
         if(this.sub_specifications.length>0){
        this.sub_specifications.clear(); 
        }  
}
refund():void{
let  is_refund =this.productForm.get('refund').value;
if(is_refund) {
    this.productForm.controls['before'].setValidators([Validators.required]);
} else {
   this.productForm.controls['before'].clearValidators();
}   
}

changeunitPrice(unit_price,i):void{
if(i==0){    
this.productForm.patchValue({
unit_price:unit_price,
}); 
}       
}

changeBasePrice(price)
{
    this.variations.controls[0].patchValue({
    price: price   
    });

}
}
