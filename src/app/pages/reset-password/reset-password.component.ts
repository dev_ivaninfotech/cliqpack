import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
// import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  regForm: any = FormGroup;
  IsSubmitted:boolean=false;
  constructor(private route: Router, private notifyService: NotificationService, private http: HttpClient, private api: ServicesService, private formBuilder: FormBuilder,private commonservice:CommonService,private spinner: NgxSpinnerService,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  this.regForm = this.formBuilder.group({
      id: [this.activatedRoute.snapshot.params.id, Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: ['', [Validators.required,Validators.minLength(6)]],
    },
    { 

      validator: ConfirmedValidator('password', 'password_confirmation')

    }
    
    );        
  }
onSubmit() {
    //console.log(this.regForm.value);
    this.IsSubmitted=true;
    if(this.regForm.status=="VALID" )
   {
        this.spinner.show();
        this.commonservice.postData(this.regForm.value,"reset-password").subscribe(data => {
             this.spinner.hide();
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "Reset Password")
               this.regForm.reset(); 
               this.IsSubmitted=false;
               this.route.navigate(['/login']);

            }
            else{
              this.notifyService.showError(data.message, "Reset Password")
        
            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
    
}
}
