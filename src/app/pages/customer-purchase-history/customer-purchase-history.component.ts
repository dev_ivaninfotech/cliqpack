import { Component, OnInit,AfterViewInit,ViewChild  } from '@angular/core';
import { NotificationService } from '../../notification.service'
import { CommonService } from '../../service/common.service';
import { Router } from '@angular/router';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";

class searchFilter {
  code: any;
  payment_status: "";
  delivery_status: "";
}
@Component({
  selector: 'app-customer-purchase-history',
  templateUrl: './customer-purchase-history.component.html',
  styleUrls: ['./customer-purchase-history.component.css']
})
export class CustomerPurchaseHistoryComponent implements OnInit {
  orderDetails:any;  
   userDetails:any;
   orders:any;
   start_index:any;
   searchForm: searchFilter = new searchFilter();
  constructor(private commonservice:CommonService,private route: Router, private notifyService: NotificationService,private confirmationDialogService: ConfirmationDialogService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  this.spinner.show();     
  let body={page_no:1};   
  this.commonservice.postData(body,"order/customerOrderList").subscribe(data => {
    this.spinner.hide();      
    if(data.status)
    {
     
      
      
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
    
           
  }
  
   
  
   cancelOrder(id)
  {
      this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to cancel this Order... ?')
    .then((confirmed) => {
     this.spinner.show();   
     let body={id:atob(id)};
     this.commonservice.postData(body,"seller/deleteOrder").subscribe(data => {
    if(data.status)
    {
     
      this.notifyService.showSuccess("This order has been cancelled successfully", "Purchase History") 
      this.spinner.hide();    
    }

    },
    err => {
      console.log("Error occured.",err)
    });    
     })
   
     .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));  
  } 

}
