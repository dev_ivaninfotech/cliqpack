import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CommonService } from '../../service/common.service';
import { NgxSpinner, Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
	categories: any = [];
	topBanners: any = [];
	Flashdeal: any = null;
	FlashdealProducts: any = [];
	biggestDeals: any = [];
	featuredProduct: any = [];
	middleBanners: any = [];
	webMiddleBanner: any = {};
	webBottomBanner: any = {};
	webFooterBanner: any = {};
	brandMaster: any = [];
	blogsMaster: any = [];
	userDetails: any;

	constructor(
		private commonservice: CommonService,
		private route: Router,
		private notifyService: NotificationService,
		private http: HttpClient,
		private api: ServicesService,
		private spinner: NgxSpinnerService
	) {

	}

	customOptions: OwlOptions;

	ngOnInit(): void {
		this.commonservice.loggeduser.subscribe(user => {
			this.userDetails = user;
		}, err => {
			console.log("Error occured.", err)
		});

		this.customOptions = {
			loop: true,
			margin: 10,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			dotsEach: false,
			autoplay: true,
			nav: true,
			dots: false,
			navText: ['', ''],
			responsive: {
				0: {
					items: 1
				},
				400: {
					items: 2
				},
				740: {
					items: 3
				},
				940: {
					items: 6
				}
			},

		}

		this.pageMasters();
	}

	pageMasters() {
		let httpOptions = {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Authorization': (window.localStorage.getItem('authtoken')) ? window.localStorage.getItem('authtoken') : ''
			})
		}

		this.http.get<any>(this.api.api_url + 'home-categoris?featured=true')
			.subscribe((res: any) => {
				// console.log(res);
				if (res.status == 200) {
					this.categories = res.data;
				} else {
					this.notifyService.showWarning("Somthing went wrong !!!!", "")
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "")
			});

		this.http.post<any>(this.api.api_url + 'products/todays-deal', {}, httpOptions)
			.subscribe((res: any) => {
				if (res.status == true) {
					this.biggestDeals = res.data.products;
				} else {
					this.notifyService.showWarning(res.message, 'Product Today-Deal API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Product Today-Deal API Error Occured");
			});

		this.http.post<any>(this.api.api_url + 'categories/left', {}, httpOptions)
			.subscribe((res: any) => {
				if (res.status == true) {
					this.topBanners = res.data.topBanners;
					this.FlashdealProducts = res.data.FlashdealProducts;

					if (res.data.Flashdeal != null) {

						const start_date: any = new Date(res.data.Flashdeal.start_date * 1000);
						const end_date: any = new Date(res.data.Flashdeal.end_date * 1000);
						const current_date: any = new Date();

						console.log('start_date' + start_date);
						console.log('end_date' + end_date);
						console.log('current_date' + current_date);

						if (current_date >= start_date && current_date <= end_date) {
							this.Flashdeal = res.data.Flashdeal;
							this.Flashdeal.timeleft = Math.round((Math.abs(end_date - current_date)) / 1000);

							setInterval(() => {
								if (this.Flashdeal.timeleft > 0) {
									this.Flashdeal.timeleft--;

									let delta = this.Flashdeal.timeleft;

									// calculate (and subtract) whole days
									let days = Math.floor(delta / 86400);
									delta -= days * 86400;

									// calculate (and subtract) whole hours
									let hours = Math.floor(delta / 3600) % 24;
									delta -= hours * 3600;

									// calculate (and subtract) whole minutes
									let minutes = Math.floor(delta / 60) % 60;
									delta -= minutes * 60;

									// what's left is seconds
									let seconds = delta % 60;  // in theory the modulus is not required

									this.Flashdeal.daysLeft = days;
									this.Flashdeal.hoursLeft = hours;
									this.Flashdeal.minutesLeft = minutes;
									this.Flashdeal.secondsLeft = seconds;
								}
							}, 1000)
						}

					}
				} else {
					this.notifyService.showWarning(res.message, 'Categories Left API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Categories Left API Error Occured");
			});

		this.http.post<any>(this.api.api_url + 'products/recent-view', {}, httpOptions)
			.subscribe((res: any) => {
				if (res.status == true) {
					this.featuredProduct = res.data.featuredProduct;
					console.log(this.featuredProduct)
				} else {
					this.notifyService.showWarning(res.message, 'Recent View API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Recent View API Error Occured");
			});

		this.http.post<any>(this.api.api_url + 'banners-top', {})
			.subscribe((res: any) => {
				if (res.status == true) {
					this.webMiddleBanner = res.data.BannerOne;
					this.webBottomBanner = res.data.BannerTwo;
					this.webFooterBanner = res.data.BannerThree;
				} else {
					this.notifyService.showWarning(res.message, 'Recent View API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Recent View API Error Occured");
			});

		this.http.get<any>(this.api.api_url + 'brands')
			.subscribe((res: any) => {
				if (res.status == true) {
					this.brandMaster = res.data
				} else {
					this.notifyService.showWarning(res.message, 'Brands API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Brands API Error Occured");
			});

		this.http.post<any>(this.api.api_url + 'blog/blog-list', { 'latest': true })
			.subscribe((res: any) => {
				if (res.status == true) {
					this.blogsMaster = res.allBlogs
				} else {
					this.notifyService.showWarning(res.message, 'Blogs API Exception');
				}
			}, (error: any) => {
				this.notifyService.showWarning("Somthing went wrong !!!!", "Blogs API Error Occured");
			});
	}

	addToWishList(item) {
		if (this.userDetails == null) {
			this.notifyService.showError("Please login first", "Wishlist")
			this.route.navigate(['/login']);
		}
		else {
			this.spinner.show();
			let body = { product_id: item.id };
			this.commonservice.postData(body, "wishlist/add").subscribe(resp => {
				if (resp.status) {
					item['iswishlist'] = 1;
					this.notifyService.showSuccess(resp.message, "Success Message");
				} else {
					this.notifyService.showError(resp.message, "Error Message");
				}
				this.spinner.hide();
			},
				err => {
					console.log("Error occured.", err)
					this.spinner.hide();
				});
		}
	}

	removeFromWishlist(item) {
		if (this.userDetails == null) {
			this.notifyService.showError("Please login first", "Wishlist")
			this.route.navigate(['/login']);
		}
		else {
			this.spinner.show();
			let body = { product_id: item.id };
			this.commonservice.postData(body, "wishlist/remove").subscribe(resp => {
				if (resp.status) {
					item['iswishlist'] = 0;
					this.notifyService.showSuccess(resp.message, "Success Message");
				} else {
					this.notifyService.showError(resp.message, "Error Message");
				}
				this.spinner.hide();
			},
				err => {
					console.log("Error occured.", err)
					this.spinner.hide();
				});
		}
	}

	parseNumber(num) {
		return parseInt(num)
	}
}