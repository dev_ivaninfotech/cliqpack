import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { NotificationService } from '../../notification.service'
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmedValidator } from '../../confirmed.validator'; 
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-customer-register',
  templateUrl: './customer-register.component.html',
  styleUrls: ['./customer-register.component.css']
})
export class CustomerRegisterComponent implements OnInit {
  // regForm = new FormGroup({
  //   name: new FormControl(''),
  //   dob: new FormControl(''),
  //   email: new FormControl(''),
  //   password: new FormControl(''),
  //   password_confirmation: new FormControl(''),

  // });

  // regForm:any;
  regForm: any = FormGroup;
  IsSubmitted:boolean=false;
  constructor(private route: Router, private notifyService: NotificationService, private http: HttpClient, private api: ServicesService, private formBuilder: FormBuilder,private commonservice:CommonService,private spinner: NgxSpinnerService) {

    
  }

  ngOnInit(): void {
  this.regForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: ['', [Validators.required,Validators.minLength(6)]],
      checkbox_example_1: ['', Validators.required]
    },
    { 

      validator: ConfirmedValidator('password', 'password_confirmation')

    }
    
    );    
  }

  onSubmit() {
    //console.log(this.regForm.value);
    this.IsSubmitted=true;
    if(this.regForm.status=="VALID" )
   {
        this.spinner.show();
        this.commonservice.postData(this.regForm.value,"auth/web/signup").subscribe(data => {
             this.spinner.hide();
            if(data.status)
            {
               this.notifyService.showSuccess(data.message, "Sign UP")
               this.regForm.reset(); 
               this.IsSubmitted=false;
            }
            else{
                    let errors=data.errors;
                    for (let error of errors) 
                    {
                     this.notifyService.showError(error, "cliqPack")
                    }
            }
            },
            err => {
              console.log("Error occured.")
            });  
   }
    
}
}
